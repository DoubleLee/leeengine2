/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "PlayerShip.h"

#include "BlasterShip.h"
#include "Engine.h"
#include "Window.h"

#include "GL/glew.h"
#include "GLFW/glfw3.h"


#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include "MathUtilities.h"

PlayerShip::PlayerShip(void)
	:
	SafeEntity(),
	mpGame(nullptr)
	{

	}


PlayerShip::~PlayerShip(void)
	{

	}

bool PlayerShip::Init(BlasterShip * pGame)
	{
	mpGame = pGame;
	return SafeEntity::Init();
	}

void PlayerShip::PlayerControls(float frameDelta)
	{
	Window * pWindow = mpGame->GetWindow();
	const float playerSpeed(30.0f);
	glm::vec3 moveVec(0.0f);
	if ( pWindow->IsKeyDown('A') )
		{
		moveVec.x -= playerSpeed;
		}

	if ( pWindow->IsKeyDown('D') )
		{
		moveVec.x += playerSpeed;
		}

	if ( pWindow->IsKeyDown('W') )
		{
		moveVec.z -= playerSpeed;
		}

	if ( pWindow->IsKeyDown('S') )
		{
		moveVec.z += playerSpeed;
		}

	moveVec *= frameDelta;

	if ( pWindow->IsKeyDown('Q') )
		{
		mWorld = rotate(mWorld, 3.14f / 4.f * frameDelta, vec3up );
		}

	if ( pWindow->IsKeyDown('E') )
		{
		mWorld = rotate(mWorld, -3.14f / 4.f * frameDelta, vec3up );
		}

	mWorld = translate(mWorld, moveVec);
	}
