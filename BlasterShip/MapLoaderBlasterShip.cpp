/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "MapLoaderBlasterShip.h"

#include "BlasterShip.h"
#include "PlayerShip.h"
#include "MathUtilities.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include <fstream>
using namespace std;

MapLoaderBlasterShip::MapLoaderBlasterShip()
	{

	}

MapLoaderBlasterShip::~MapLoaderBlasterShip()
	{

	}

bool MapLoaderBlasterShip::LoadMap(BlasterShip * pGame, string file)
	{
	mpGame = pGame;

	return TextMapLoader::LoadMap( pGame, file );
	}

FindResult MapLoaderBlasterShip::FindVirtualCommand(string &command, string &data, ifstream &ifs)
	{
	if ( command == "player" )
		{
		unsigned int mesh, tex, prog;
		ifs >> mesh;
		ifs >> tex;
		ifs >> prog;

		vec3 pos;
		ifs >> pos.x;
		ifs >> pos.y;
		ifs >> pos.z;

		if ( ifs.bad() )
			{
			return FindResult::ResultError;
			}

		UniquePointer<PlayerShip> pPlayer( new PlayerShip() );

		pPlayer->Init(mpGame);

		pPlayer->mWorld = translate( mat4identity, pos );
		pPlayer->SetMesh( mMeshes.at(mesh) );
		pPlayer->SetTexture( mTextures.at(tex) );
		pPlayer->SetProgram( mPrograms.at(prog) );

		mpGame->mpPlayer = std::move(pPlayer);

		return FindResult::ResultFound;
		}
	else
		{
		return TextMapLoader::FindVirtualCommand(command, data, ifs);
		}
	}
