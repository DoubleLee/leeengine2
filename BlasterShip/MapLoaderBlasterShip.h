/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#ifndef MAPLOADERBLASTERSHIP_H
#define MAPLOADERBLASTERSHIP_H

class BlasterShip;

#include "TextMapLoader.h"
class MapLoaderBlasterShip : public TextMapLoader
{

public:
	MapLoaderBlasterShip();
	virtual ~MapLoaderBlasterShip();

	bool LoadMap(BlasterShip * pGame, string file );

	virtual FindResult FindVirtualCommand(string &command, string &data, ifstream &ifs);

protected:
	BlasterShip * mpGame;
};

#endif // MAPLOADERBLASTERSHIP_H
