/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/
#pragma once
#include "SafeEntity.h"

class BlasterShip;

class PlayerShip : public SafeEntity
{
public:
	PlayerShip(void);
	virtual ~PlayerShip(void);

    bool Init(BlasterShip * pGame);

	void PlayerControls(float frameDelta);

protected:
    BlasterShip * mpGame;
};

