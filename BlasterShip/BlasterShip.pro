TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += GLM_FORCE_RADIANS

SOURCES += \
	BlasterShip.cpp \
	PlayerShip.cpp \
	Main.cpp \
	MapLoaderBlasterShip.cpp

HEADERS += \
	BlasterShip.h \
	PlayerShip.h \
	MapLoaderBlasterShip.h

INCLUDEPATH += $$PWD/../LeeEngine2
DEPENDPATH += $$PWD/../LeeEngine2

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../LeeEngine2/libLeeEngine2.a
unix:!macx: LIBS += -L$$OUT_PWD/../LeeEngine2/ -lLeeEngine2

unix:!macx: LIBS += -lGLEW -lGLU -lGL -lglfw3 -lfreeimage -lassimp -lopenal -lalut -lpthread -ldl -lX11 -lXrandr -lXxf86vm -lXi

unix:!macx: QMAKE_CXXFLAGS += -std=c++11

