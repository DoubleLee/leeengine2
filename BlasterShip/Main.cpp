/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "BlasterShip.h"

int main()
	{
	BlasterShip blasterShip;
	if ( blasterShip.Init() )
		{
		return blasterShip.Run();
		}
	else
		{
		return 1;
		}
	}