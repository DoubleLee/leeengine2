/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/
#pragma once

#include "SmartPointers.h"

class PlayerShip;

#include "Engine.h"

class BlasterShip : public Engine
{
friend class MapLoaderBlasterShip;
public:
	BlasterShip();
	virtual ~BlasterShip();

	virtual bool Init(int width = 1024, int height = 768);

	virtual void GameUpdate();
	virtual void GameDraw();
protected:
	UniquePointer<PlayerShip> mpPlayer;

private:
	BlasterShip( const BlasterShip & other );
	BlasterShip & operator = ( const BlasterShip & other );
	BlasterShip( BlasterShip && other );
	BlasterShip & operator = ( BlasterShip && other );

};

