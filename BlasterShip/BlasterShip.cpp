/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "BlasterShip.h"

#include "MeshData.h"
#include "TextureData.h"
#include "Engine.h"
#include "Logger.h"
#include "SafeEntity.h"
#include "Camera.h"
#include "ShaderProgram.h"
#include "ResourceManager.h"
#include "PlayerShip.h"
#include "OrientedBB.h"
#include "MathUtilities.h"
#include "Skybox.h"
#include "MapLoaderBlasterShip.h"
#include "StrUtils.h"
#include "EngineSettingsLoader.h"
#include "Window.h"

#include <map>
#include <fstream>
using namespace std;


#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include "GLFW/glfw3.h"

#include "Entity.h"

BlasterShip::BlasterShip()
	:
	Engine(),
	mpPlayer(nullptr)
	{

	}

BlasterShip::~BlasterShip()
	{

	}

bool BlasterShip::Init(int width, int height)
	{
	LogFuncBegin()

	if ( !Engine::Init(width, height) )
		{
		LogFailure("Engine failed to load")
		return false;
		}

	string mapPath = "../../LeeEngine2SubDirs/Art/Maps/SimpleMap1.txt";
	ToPlatformPath(mapPath);

	MapLoaderBlasterShip loader;

	if ( !loader.LoadMap(this, mapPath) )
		{
		LogFailure("Map failed to load")
		return false;
		}

	mpCamera->SetView(vec3(0.0f, 50.0f, 0.0f), vec3zero, vec3front );
	int x,y;
	GetWindow()->GetWindowSize(&x,&y);
	mpCamera->SetProj( float(3.14 / 4.0), float(x) / float(y), 1.0f, 100000.0f );

	LogFuncEndSuccess()
	return true;
	}

void BlasterShip::GameUpdate()
	{
	// exit if escape is pressed
	if ( mpWindow->IsKeyDown(GLFW_KEY_ESCAPE) )
		{
		mpWindow->CloseWindow();
		return;
		}

	mWindowTitle += "FPS ";
	mWindowTitle += to_string(mFramesPerSec);

	mpPlayer->PlayerControls(mFrameDeltaF);

	// update each entity
	int count = 0;
	for ( SharedPointer<SafeEntity> & pIndex : mEntities )
		{
		pIndex->mWorld = rotate( pIndex->mWorld, ( (count % 2 == 0) ? ONEF : -ONEF) * 3.14f / 4.0f * mFrameDeltaF, vec3up );
		++count;
		}

	// update the Obbs
	mpPlayer->UpdateOBB();
	for ( SharedPointer<SafeEntity> & pIndex : mEntities )
		{
		pIndex->UpdateOBB();
		}

	OrientedBB * pPlayer = mpPlayer->mpOBB;
	OrientedBB * pOther = nullptr;
	for ( unsigned int i = 0; i < mEntities.size(); ++i )
		{
		pOther = mEntities[i]->mpOBB;
		if ( pOther == nullptr )
			continue;
		if ( pPlayer->IsColliding( *pOther ) )
			{
			((mWindowTitle += " Collision with entity") += to_string(i)) += ' ';
			}
		}

	array<Orientation, 6> playerOrients = mpCamera->mViewFrustum.GetPlaneOrientationsForOBB( *mpPlayer->mpOBB );

	bool foundAnOutside = false;

	if ( playerOrients[ViewFrustum::Planes::TOP] != Orientation::POSITIVE_SIDE )
		{
		mWindowTitle += " outside of top plane ";
		foundAnOutside = true;
		}

	if ( playerOrients[ViewFrustum::Planes::RIGHT] != Orientation::POSITIVE_SIDE )
		{
		mWindowTitle += " outside of right plane ";
		foundAnOutside = true;
		}

	if ( playerOrients[ViewFrustum::Planes::BOTTOM] != Orientation::POSITIVE_SIDE )
		{
		mWindowTitle += " outside of bottom plane ";
		foundAnOutside = true;
		}

	if ( playerOrients[ViewFrustum::Planes::LEFT] != Orientation::POSITIVE_SIDE )
		{
		mWindowTitle += " outside of left plane ";
		foundAnOutside = true;
		}

	if ( !foundAnOutside )
		{
		mWindowTitle += " inside all planes ";
		}

	// rotate skybox a little
	if ( mpSkybox )
		{
		mpSkybox->mRotations = rotate(mpSkybox->mRotations, -(3.14f / 300.f) * mFrameDeltaF, vec3right);
		}

	}

void BlasterShip::GameDraw()
	{
	// draw skybox first
	if ( mpSkybox )
		{
		mpSkybox->Draw(mpCamera.get());
		}

	int objectsCulled = 0;
	if ( !mpCamera->mViewFrustum.ShouldBeCulled(*mpPlayer->mpOBB) )
		{
		mpPlayer->Draw(mpCamera.get());
		}
	else
		{
		++objectsCulled;
		}

	for ( SharedPointer<SafeEntity> & index : mEntities )
		{
		if ( !mpCamera->mViewFrustum.ShouldBeCulled(*index->mpOBB) )
			{
			index->Draw(mpCamera.get());
			}
		else
			{
			++objectsCulled;
			}
		}

	(mWindowTitle += " objects culled ") += to_string(objectsCulled);
	}
