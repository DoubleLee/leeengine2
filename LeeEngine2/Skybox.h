/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include <string>

#include "SafeEntity.h"

class Skybox : public SafeEntity
{
public:
	Skybox();
	~Skybox();

	// Special overload of SafeEntities draw call to take care
	// of custom draw code for a Skybox.
	void Draw( Camera * pCamera );

	mat4 mRotations;
protected:
	
};

