/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

namespace Rand
{
void SeedRandomEngines();
unsigned int GetRandomUnsignedInt( unsigned int low, unsigned int high );
unsigned long long GetRandomUnsignedLongLongRange(unsigned long long low, unsigned long long high);
double GetRandomDoubleRange(double low, double high);
}
