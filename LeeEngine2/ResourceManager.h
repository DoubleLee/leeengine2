/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include "Logger.h"
#include <map>
#include <unordered_map>
#include <vector>
#include "ShaderProgram.h"
using std::map;

#include "SmartPointers.h"

template < class ResType > class ResourceManager
{
public:
	ResourceManager(void);
	virtual ~ResourceManager(void);

	// This method will check to see if a requested resource is already loaded,
	// if it is it will return a SharedPointer ( reference counted )
	// to that resource
	// if there is not already a copy of that resource loaded
	// it will attempt to load it.
	// returning a null pointer means the resource wasn't found, and/or couldn't be loaded
	// should be handled as a serious error in most cases
	SharedPointer<ResType> GetOrLoadFromFile( const std::string & fileName );

	//weak_ptr<ResType> GetResourceFromID( ResourceID & resID );

	// calls clear on the internall collection of resources
	// note: they are reference counted, so the memory may not be freed after this call
	// if other references exist.
	void DestroyResources();
	// calls ReleaseResource on all managed resources, this will free all resources
	void ReleaseResources();
	// calls ReloadFromFile on each resource, this will reload all resources including those that had been Released
	void ReloadResources();
protected:
	// internal class call to attempt loading of a resource
	SharedPointer<ResType> AttemptLoad( const std::string & fileName );

	std::unordered_map<string, SharedPointer<ResType> > mResMap;

private:
	// disable copying
	ResourceManager( const ResourceManager & other );
	ResourceManager( ResourceManager && other );
	ResourceManager & operator = ( const ResourceManager & other );
	ResourceManager & operator = ( ResourceManager && other );

};

template < class ResType > class ResourceManagerTwoFiles : public ResourceManager<ResType>
{
public:
	ResourceManagerTwoFiles();
	SharedPointer<ResType> GetOrLoadFromFile( const std::string & fileName ) = delete;
	SharedPointer<ResType> GetOrLoadFromFiles( const std::string & first, const std::string & second);


private:
	SharedPointer<ResType> AttemptLoad( const std::string & fileName ) = delete;
	SharedPointer<ResType> AttemptLoad( const std::string & first, const std::string & second );

	// disable copying
	ResourceManagerTwoFiles( const ResourceManagerTwoFiles & other );
	ResourceManagerTwoFiles( ResourceManagerTwoFiles && other );
	ResourceManagerTwoFiles & operator = ( const ResourceManagerTwoFiles & other );
	ResourceManagerTwoFiles & operator = ( ResourceManagerTwoFiles && other );
};


template < class ResType > ResourceManager<ResType>::ResourceManager()
	{

	}

template < class ResType > ResourceManager<ResType>::~ResourceManager()
	{

	}

template < class ResType > ResourceManagerTwoFiles<ResType>::ResourceManagerTwoFiles()
	:
	ResourceManager<ResType>()
	{

	}

template < class ResType > void ResourceManager<ResType>::ReleaseResources()
	{
	auto iter = mResMap.begin();
	auto end = mResMap.end();

	while ( iter != end )
		{
		(*iter).second->ReleaseResources();
		++iter;
		}
	}

template < class ResType > void ResourceManager<ResType>::ReloadResources()
	{
	auto iter = mResMap.begin();
	auto end = mResMap.end();

	while ( iter != end )
		{
		(*iter).second->ReloadFromFile();
		++iter;
		}
	}

template < class ResType > void ResourceManager<ResType>::DestroyResources()
	{
	mResMap.clear();
	}


//template < class ResType > ResTypeWP ResourceManager<ResType>::GetResourceFromID( ResourceID & resID )
//	{
//	auto iter = mResMap.find( resID );
//	if ( iter == mResMap.end() )
//		{
//		resID = 0;
//		return nullptr;
//		}
//	else
//		{
//		return iter->second.get();
//		}
//	}

//template < class ResType > bool ResourceManager<ResType>::IsValidID(ResourceID & resID)
//	{
//	auto iter = mResMap.find( resID );
//	if ( iter == mResMap.end() )
//		{
//		resID = 0;
//		return false;
//		}
//	else
//		{
//		return true;
//		}
//	}

template < class ResType> SharedPointer<ResType> ResourceManager<ResType>::GetOrLoadFromFile( const std::string & fileName )
	{
	if ( !mResMap.empty() )
		{
		auto index = mResMap.begin();
		auto end = mResMap.end();

		while ( index != end )
			{
			if ( index->first == fileName )
				{
				return index->second;
				}
			++index;
			}
		}

	return AttemptLoad(fileName);
	}

template < class ResType > SharedPointer<ResType> ResourceManager<ResType>::AttemptLoad( const std::string & fileName )
	{
	SharedPointer<ResType> pRes( new ResType() );

	if ( pRes->LoadFromFile( fileName ) )
		{
		mResMap.insert( std::make_pair( fileName, pRes ) );
		return pRes;
		}
	else
		{
		LogFailure("Failed to load resource file " + fileName)
		return SharedPointer<ResType>(nullptr);
		}
	}

template < class ResType> SharedPointer<ResType> ResourceManagerTwoFiles<ResType>::GetOrLoadFromFiles( const std::string & firstFile, const std::string & secondFile )
	{
	if ( !this->mResMap.empty() )
		{
		auto index = this->mResMap.begin();
		auto end = this->mResMap.end();

		while ( index != end )
			{
			if ( index->first == firstFile )
				{
				return index->second;
				}
			++index;
			}
		}

	return AttemptLoad(firstFile, secondFile);
	}

template < class ResType > SharedPointer<ResType> ResourceManagerTwoFiles<ResType>::AttemptLoad( const std::string & firstFile, const std::string & secondFile )
	{
	SharedPointer<ResType> pRes( new ResType() );

	if ( pRes->LoadFromFiles( firstFile, secondFile ) )
		{
		this->mResMap.insert( std::make_pair( firstFile, pRes ) );
		return pRes;
		}
	else
		{
		LogFailure("Failed to load resource file " + firstFile)
		return SharedPointer<ResType>(nullptr);
		}
	}
