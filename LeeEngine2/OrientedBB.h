/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"
using namespace glm;

class OrientedBB
{
public:
	OrientedBB(const vec3 & pos, const mat3 & rots, const vec3 & extents);
	~OrientedBB();

	// takes in two OBB's and returns if they are colliding
	static bool IsColliding( const OrientedBB & first, const OrientedBB & second );
	// takes in the data for two obbs and returns if they are colliding
	static bool IsColliding( const vec3 & pos, const mat3 & rots, const vec3 & extentsA,
					 const vec3 & posB, const mat3 & rotsB, const vec3 & extentsB );
	// method for detecting OBB collision
	bool IsColliding( const OrientedBB & other );

	vec3 mPos;
	mat3 mMatRots;
	vec3 mExtents;
};
