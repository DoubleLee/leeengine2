/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Entity.h"

#include "MeshData.h"
#include "TextureData.h"
#include "Camera.h"
#include "ShaderProgram.h"

#include "GL/glew.h"

Entity::Entity(void)
	:
	mpMesh(nullptr),
	mpTexture(nullptr),
	mpShaderProgram(nullptr)
	{

	}


Entity::~Entity(void)
	{

	}

void Entity::Draw( Camera * pCamera )
	{
	if ( mpMesh && mpTexture && mpShaderProgram && pCamera )
		{
		mpMesh->Bind();
		mpTexture->Bind();

		mpShaderProgram->Bind();

		glm::mat4 MVP = *pCamera * mWorld;
		mpShaderProgram->SetUniform("MVP", MVP);

		glDrawElements( GL_TRIANGLES, mpMesh->mVertIndexCount, GL_UNSIGNED_INT, nullptr );
		}
	}
