/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Skybox.h"

#include "Logger.h"

#include "GL/glew.h"


#include "Camera.h"

#include "MeshData.h"
#include "TextureData.h"
#include "ShaderProgram.h"

#include "MathUtilities.h"


Skybox::Skybox(void)
	{
	}


Skybox::~Skybox(void)
	{
	}

void Skybox::Draw( Camera * pCamera)
	{
	if ( mpMeshData->IsLoaded() && mpTextureData->IsLoaded() && mpProgram->IsLoaded() && pCamera )
		{
		mpMeshData->Bind();
		mpTextureData->Bind();
		mpProgram->Bind();
		glCullFace( GL_FRONT );
		glDepthMask( GL_FALSE );
		mWorld = translate( mat4identity, pCamera->GetPos() );

		// TODO: shoudln't have to scale it manually, box should be recreated in blender bigger.
		auto scale = glm::scale(mat4identity, vec3(5.0f) );
		
		glm::mat4 MVP = (*pCamera) * mWorld * mRotations * scale;
		
		mpProgram->SetUniform("MVP", MVP);

		glDrawElements( GL_TRIANGLES, mpMeshData->mVertIndexCount, GL_UNSIGNED_INT, nullptr );
		glCullFace( GL_BACK );
		glDepthMask( GL_TRUE ); 
		}

	}
