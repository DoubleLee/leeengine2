/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"

class Engine;

#include "SmartPointers.h"

class MeshData;
class TextureData;
class ShaderProgram;
class Camera;
class OrientedBB;

// This class only use SharedPointer
// will not leak memory or reference something invalid
class SafeEntity
{
public:
	SafeEntity();
	virtual ~SafeEntity(void);

	bool Init();

	SharedPointer<MeshData> GetMesh();
	SharedPointer<TextureData> GetTexture();
	SharedPointer<ShaderProgram> GetProgram();

	void SetMesh( SharedPointer<MeshData> & pMeshData );
	void SetTexture( SharedPointer<TextureData> & pTextureData );
	void SetProgram( SharedPointer<ShaderProgram> & pProgram );

	// Updates the obb with any new matrix info
	void UpdateOBB();

	// draws the Entity
	void Draw( Camera * pCamera );

	glm::mat4 mWorld;
	OrientedBB * mpOBB;
protected:
	SharedPointer<MeshData> mpMeshData;
	SharedPointer<TextureData> mpTextureData;
	SharedPointer<ShaderProgram> mpProgram;
};

