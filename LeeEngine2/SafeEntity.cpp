/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "SafeEntity.h"

#include "Engine.h"
#include "ShaderProgram.h"
#include "MeshData.h"
#include "TextureData.h"
#include "ShaderProgram.h"

#include "Logger.h"
#include "Engine.h"
#include "ResourceManager.h"
#include "Camera.h"


#include "GL/glew.h"

SafeEntity::SafeEntity()
	:
	mWorld(1.0f),
	mpOBB( nullptr ),
	mpMeshData(nullptr),
	mpTextureData(nullptr),
	mpProgram(nullptr)
	{

	}

SafeEntity::~SafeEntity(void)
	{
	if ( mpOBB )
		{
		delete mpOBB;
		mpOBB = nullptr;
		}
	}

bool SafeEntity::Init()
	{
	return true;
	}

SharedPointer<MeshData> SafeEntity::GetMesh()
	{
	return mpMeshData;
	}

SharedPointer<TextureData> SafeEntity::GetTexture()
	{
	return mpTextureData;
	}

SharedPointer<ShaderProgram> SafeEntity::GetProgram()
	{
	return mpProgram;
	}

void SafeEntity::SetMesh( SharedPointer<MeshData> & pMeshData )
	{
	mpMeshData = pMeshData;
	if ( mpOBB )
		{
		delete mpOBB;
		mpOBB = nullptr;
		}

	mpOBB = new OrientedBB( (vec3)mWorld[3], (mat3)mWorld, mpMeshData->mExtents );
	}

void SafeEntity::SetTexture( SharedPointer<TextureData> & pTextureData )
	{
	mpTextureData = pTextureData;
	}

void SafeEntity::SetProgram( SharedPointer<ShaderProgram> & pProgram )
	{
	mpProgram = pProgram;
	}

void SafeEntity::UpdateOBB()
	{
	if ( mpOBB )
		{
		mpOBB->mPos = (vec3)mWorld[3];
		mpOBB->mMatRots = (mat3)mWorld;
		}
	}

void SafeEntity::Draw( Camera * pCamera )
	{
	if ( mpMeshData && mpTextureData && mpProgram && pCamera )
		{
		(*mpMeshData).Bind();
		(*mpTextureData).Bind();
		(*mpProgram).Bind();

		mpProgram->SetUniform("MVP", *pCamera * mWorld);
		glDrawElements( GL_TRIANGLES, mpMeshData->mVertIndexCount, GL_UNSIGNED_INT, nullptr );
		}
	}
