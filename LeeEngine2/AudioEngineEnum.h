/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once
#ifndef AUDIOENGINEENUM_H
#define AUDIOENGINEENUM_H

namespace Registered
{

enum AudioEngineEnum
{
EmptyAudio = 0,
Click,
MouseOverButton,
LastAudioEngine
};

}

#endif // AUDIOENGINEENUM_H
