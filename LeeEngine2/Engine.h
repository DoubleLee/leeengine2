/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include <string>
#include <vector>
using std::string;
using std::vector;

#include "SmartPointers.h"

class SafeEntity;
class MeshData;
class TextureData;
class ShaderProgram;
class Skybox;
class Camera;
class AudioManager;
class EngineSettingsLoader;
class Window;

struct GLFWwindow;

template < class ResType > class ResourceManager;
template < class ResType > class ResourceManagerTwoFiles;

class Engine
{
public:
	Engine();
	virtual ~Engine();

	// setup
	virtual bool Init(int width = 800, int height = 600);
	int Run();

	// sets up the window and openGL context
	// note: currently uses glfw3
	bool SetupWindowAndContext(const EngineSettingsLoader * pSettings);
	// sets up the openGL function pointers from the driver
	// note: currently uses glew
	bool SetupOpenGLFunctionPointers();
	// uses openAL from Creative or openAL soft depending on the library available
	bool SetupOpenALAndALUT();

	// registers the engine names
	void RegisterAudioEngineEnums();
	// empty on purpose, should be called virtual into a Game class method for
	// registering audio names
	virtual void RegisterAudioGameEnums();

	// clean up code
	void ReleaseAllApis();
	// cleans up window and context
	void ReleaseWindowAndContext();
	// cleans up the function pointers library
	void ReleaseOpenGLFunctionPointers();
	// releases openal and alut
	void ReleaseOpenAL();

	// Code that hands control to the game

	// This is where the game is handed control for updating itself for a frame
	virtual void GameUpdate() = 0;
	// This is where the game is handed control from drawing itself for a frame
	virtual void GameDraw() = 0;

	// Getters
	Window * GetWindow() const;

	// changes the screen size to what you send in

	UniquePointer<AudioManager> mpAudioMan;
	UniquePointer<ResourceManager<MeshData> > mpMeshMan;
	UniquePointer<ResourceManager<TextureData> > mpTexMan;
	UniquePointer<ResourceManagerTwoFiles<ShaderProgram> > mpProgMan;
	UniquePointer<Skybox> mpSkybox;
	vector< SharedPointer< SafeEntity > > mEntities;
protected:
	UniquePointer<Window> mpWindow;
	UniquePointer<Camera> mpCamera;
	string mWindowTitle;
	int mReturnVal;

	double mFrameStampD;
	double mFrameDeltaD;

	float mFrameStampF;
	float mFrameDeltaF;

	int mFramesPerSec;
private:
	Engine( const Engine & other );
	Engine & operator = ( const Engine & other );
};

