#include "Window.h"

#include "Logger.h"
#include "EngineSettingsLoader.h"

#include "GLFW/glfw3.h"

#include <cmath>

Window::Window()
	{

	}

bool Window::Init(const EngineSettingsLoader * pSettings)
	{
	LogFuncBegin()
	if ( !glfwInit() )
		{
		LogFailure("glfwInit failed to init")
		return false;
		}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3 );
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3 );
	glfwWindowHint(GLFW_OPENGL_CORE_PROFILE, GL_TRUE);
	glfwWindowHint(GLFW_SAMPLES, pSettings->GetSamplesAA() );

	mpWindow = glfwCreateWindow( pSettings->GetScreenWidth(), pSettings->GetScreenHeight(), "GLFW3", NULL, NULL );

	if ( !mpWindow )
		{
		LogFailure("glfw3 create window failed to create window")
		glfwTerminate();
		return false;
		}

	glfwMakeContextCurrent(mpWindow);

	glfwSetWindowTitle(mpWindow, "LeeEngine2");

	LogFuncEndSuccess()
	return true;
	}

void Window::ChangeScreenSize( const int width, const int height )
	{
	glfwSetWindowSize(mpWindow, width, height );
	glViewport(0,0,width,height);
	}

void Window::SwapBuffers()
	{
	glfwSwapBuffers(mpWindow);
	}

bool Window::ShouldClose()
	{
	return glfwWindowShouldClose(mpWindow) == GL_TRUE;
	}

void Window::SetWindowTitle(const string & str)
	{
	glfwSetWindowTitle(mpWindow, str.c_str() );
	}

bool Window::IsKeyDown(int key)
	{
	return glfwGetKey(mpWindow, key) == GLFW_PRESS;
	}

bool Window::IsMousebuttonDown( int button )
	{
	return glfwGetMouseButton( mpWindow, button );
	}

void Window::GetCursorPos(int *pX, int *pY)
	{
	double x, y;
	glfwGetCursorPos(mpWindow, &x, &y);
	*pX = int(floor(x));
	*pY = int(floor(y));
	}

void Window::SetCursorPos( int x, int y )
	{
	glfwSetCursorPos(mpWindow, x, y);
	}

void Window::GetWindowSize(int *pX, int *pY)
	{
	glfwGetWindowSize(mpWindow, pX, pY);
	}

void Window::CloseWindow()
	{
	glfwSetWindowShouldClose( mpWindow, GL_TRUE );
	}
