/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Plane.h"

#include "OrientedBB.h"

Plane::Plane()
	:
	mNormal(0.0f),
	mDot(0.0f)
	{

	}

Plane::Plane(const vec3 & a, const vec3 & b, const vec3 & c)
	:
	mNormal(0.0f),
	mDot(0.0f)
	{
	mNormal = normalize(cross( b - a, c - a) );
	mDot = dot( mNormal, a );
	}

Plane::~Plane(void)
	{

	}

Plane Plane::ComputePlane(const vec3 & a, const vec3 & b, const vec3 & c)
	{
	return Plane( a , b , c );
	}

// Test if OBB b intersects plane p
bool Plane::Collides(const OrientedBB & obb)
	{
	float projectedRadius,boxCenterToPlane;
	CalculateDistances( obb, projectedRadius, boxCenterToPlane );
	return abs(boxCenterToPlane) <= projectedRadius;
	}

Orientation Plane::GetPlaneBoxOrientation( const OrientedBB & obb )
	{
	float projectedRadius,boxCenterToPlane;
	CalculateDistances( obb, projectedRadius, boxCenterToPlane );
	if ( boxCenterToPlane <= -projectedRadius )
		{
		return Orientation::NEGATIVE_SIDE;
		}
	else if ( projectedRadius <= boxCenterToPlane )
		{
		return Orientation::POSITIVE_SIDE;
		}
	else
		{
		return Orientation::IN_SIDE;
		}
	}

void Plane::CalculateDistances( const OrientedBB & obb, float & projectedRadius, float & boxCenterToPlane )
	{
	// Compute the projection interval radius of b onto L(t) = b.c + t * p.n
	projectedRadius = obb.mExtents[0]*abs(dot(mNormal, obb.mMatRots[0])) +
	obb.mExtents[1]*abs(dot(mNormal, obb.mMatRots[1])) +
	obb.mExtents[2]*abs(dot(mNormal, obb.mMatRots[2]));
	// Compute distance of box center from plane
	boxCenterToPlane = dot(mNormal, obb.mPos) - mDot;
	// Intersection occurs when distance s falls within [-r,+r] interval
	}
