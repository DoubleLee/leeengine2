/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include <string>
using std::string;

// Base class for Resources
class Resource
{
public:
	Resource(void);
	virtual ~Resource(void);

	// virtually load resource from file
	virtual bool LoadFromFile( const string & fileName ) = 0;
	// if this resource was once loaded with something, and the fileName string wasn't modified after
	// this call can use that stored file name to reload the resource
	virtual bool ReloadFromFile( );
	// returns if the resource is loaded
	bool IsLoaded() const;
	// virtual bind whatever this resource type is
	virtual void Bind() const = 0;

	// returns file name of resource
	const string & GetFileName() const;

	// releases the resource's data.
	virtual void ReleaseResources( ) = 0;
	
protected:
	bool mIsLoaded;
	string mFileName;
};

