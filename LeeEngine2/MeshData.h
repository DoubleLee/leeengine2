/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include "Resource.h"

#include "OrientedBB.h"


#include "glm/gtx/vec1.hpp"

class MeshData : public Resource
{
public:
	MeshData();
	virtual ~MeshData();

	// Loads a mesh from file,
	// internally uses AssImp 
	// so it supports LOTS of formats
	virtual bool LoadFromFile( const std::string & file );
	// Releases the mesh's data
	virtual void ReleaseResources();
	// bind the mesh to the renderer
	virtual void Bind() const;

	unsigned int mVAO;
	unsigned int mVBOVertPositions;
	unsigned int mVBOVertNormals;
	unsigned int mVBOVertUVs;

	unsigned int mVBOVertIndices;

	unsigned int mVertCount;
	unsigned int mVertIndexCount;
	
	vec3 mExtents;
protected:
	
private:
	MeshData( const MeshData & other );
	MeshData & operator = ( const MeshData & other );
};

