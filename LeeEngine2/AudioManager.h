/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include "AudioEngineEnum.h"

#include <vector>
#include <string>
using std::vector;
using std::string;

#include "SmartPointers.h"

class AudioData;
class AudioSource;
class AudioListener;

#include "glm/glm.hpp"
using namespace glm;

class RegisteredAudio
{
public:
	RegisteredAudio(int soundID);
	Registered::AudioEngineEnum mAudioID;
	vector< UniquePointer<AudioData> > mRelatedAudioFiles;

	bool operator == ( const RegisteredAudio & other );
};

class AudioManager
{
public:
	AudioManager();
	~AudioManager();
	void AddRegisteredName( int nameID );

	// Add a new audio entry in registered audio names ( enum )
	bool AddRegisteredAudio( int audioID, AudioData * pAudioData );
	// plays random entry from registered audio name ( enum )
	void PlayRegisteredAudio( int audioID );
	// plays random entry from registered audio name ( enum ) at specific world cordinates
	void PlayRegisteredAudio( int audioID, const vec3 & pos, bool relative = false );

	// Main update function for in class management
	void Update();
	// Set the world cordinate of the listener
	void SetListenerPos( const vec3 & pos );
	void SetListenerOrient( const vec3 & front, const vec3 & up);
	void SetListenerPosAndOrient( const vec3 & pos, const vec3 & front, const vec3 & up );

	UniquePointer<AudioListener> mpListener;

protected:
	vector< RegisteredAudio > mRegisteredAudio;
	vector< UniquePointer< AudioSource > > mQuickPlaySources;
};

#endif // AUDIOMANAGER_H
