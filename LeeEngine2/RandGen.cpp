/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "RandGen.h"

#include <random>
#include <chrono>
using namespace std;
using namespace chrono;

namespace Rand
{
mt19937 smallEngine;
mt19937_64 largeEngine;




void SeedRandomEngines()
	{
	auto timePoint = high_resolution_clock::now().time_since_epoch();
	smallEngine.seed( (unsigned long)timePoint.count() );
	largeEngine.seed( (unsigned long)timePoint.count() );
	}

unsigned int GetRandomUnsignedInt(unsigned int low, unsigned int high)
	{
	uniform_int_distribution<unsigned int> dist(low,high);
	return dist(smallEngine);
	}

unsigned long long GetRandomUnsignedLongLongRange(unsigned long long low, unsigned long long high)
	{
	uniform_int_distribution<unsigned long long> dist(low,high);
	return dist(largeEngine);
	}

double GetRandomDoubleRange( double low, double high )
	{
	uniform_real_distribution<double> dist(low,high);
	return dist(largeEngine);
	}

}
