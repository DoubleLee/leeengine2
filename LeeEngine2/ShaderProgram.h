/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include <string>
using std::string;


#include "glm/glm.hpp"

// TODO: Make the shader program class cache the uniform id's and their string names
// TODO: clean this class up to make it a Resource child
class ShaderProgram
{
public:
	ShaderProgram();
	~ShaderProgram();

	bool LoadFromFiles( const string & vertFilePath, const string & fragFilePath );
	void Bind();

	void ReleaseResources();

	void SetUniform( const char * pUniformName, const glm::mat4 & mat );
	void SetUniform( const char * pUniformName, const glm::vec3 & vec3 );
	void SetUniform( const char * pUniformName, const float f );
	void SetUniform( const char * pUniformName, const int i );
	void SetUniform( const char * pUniformName, const bool b );

    bool IsLoaded() const;

    unsigned int GetProgramID() const;
protected:
	string mVertFile;
	string mFragFile;

	unsigned int mVertID;
	unsigned int mFragID;

	unsigned int mProgramID;

	bool mProgramLoaded;

private:
	ShaderProgram( const ShaderProgram & other );
	ShaderProgram & operator = ( const ShaderProgram & other );
};

