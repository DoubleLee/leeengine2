#ifndef WINDOW_H
#define WINDOW_H

#include <string>
using std::string;

class EngineSettingsLoader;
struct GLFWwindow;

class Window
{
public:
	Window();

	bool Init( const EngineSettingsLoader * pSettings );

	void ChangeScreenSize( const int width, const int height );

	// swap the render buffer to present the newly drawn frame
	void SwapBuffers();

	// sets the window title
	void SetWindowTitle(const string & str);
	// check for key's pressed
	bool IsKeyDown(int key);
	// check for mouse button down
	bool IsMousebuttonDown( int button );
	// returns the cursors position
	void GetCursorPos( int * pX, int * pY);
	// sets the cursors position
	void SetCursorPos( int x, int y );
	// returns the windows size
	void GetWindowSize( int * pX, int * pY );
	// closes the window
	void CloseWindow();
	// returns true if the game should close.
	bool ShouldClose();

private:
	GLFWwindow * mpWindow;
};

#endif // WINDOW_H
