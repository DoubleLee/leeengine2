#-------------------------------------------------
#
# Project created by QtCreator 2013-08-01T11:44:22
#
#-------------------------------------------------

QT       -= core gui

TARGET = LeeEngine2
TEMPLATE = lib
CONFIG += staticlib

DEFINES += GLM_FORCE_RADIANS

SOURCES += \
	ViewFrustum.cpp \
	TextureData.cpp \
	MathUtilities.cpp \
	Skybox.cpp \
	ShaderProgram.cpp \
	SafeEntity.cpp \
	ResourceManager.cpp \
	Resource.cpp \
	RandGen.cpp \
	Plane.cpp \
	OrientedBB.cpp \
	AudioSource.cpp \
	AudioListener.cpp \
	MeshData.cpp \
	Logger.cpp \
	Entity.cpp \
	Engine.cpp \
	Camera.cpp \
	AudioData.cpp \
	StrUtils.cpp \
	TextMapLoader.cpp \
	AudioManager.cpp \
    EngineSettingsLoader.cpp \
    Window.cpp

HEADERS += \
	ViewFrustum.h \
	TextureData.h \
	Skybox.h \
	ShaderProgram.h \
	SafeEntity.h \
	ResourceManager.h \
	Resource.h \
	RandGen.h \
	Plane.h \
	OrientedBB.h \
	AudioListener.h \
	MeshData.h \
	MathUtilities.h \
	Logger.h \
	Entity.h \
	Engine.h \
	Camera.h \
	AudioSource.h \
	AudioData.h \
	StrUtils.h \
	TextMapLoader.h \
	AudioManager.h \
	AudioEngineEnum.h \
    SmartPointers.h \
    EngineSettingsLoader.h \
    Window.h
unix:!symbian {
	maemo5 {
		target.path = /opt/usr/lib
	} else {
		target.path = /usr/lib
	}
	INSTALLS += target
}

QMAKE_CXXFLAGS += -std=c++11

