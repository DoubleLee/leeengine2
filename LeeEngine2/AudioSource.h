/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"

class AudioSource
{
public:
	AudioSource(void);
	~AudioSource(void);

	bool LoadSource();
	void ReleaseResources();

	void Play( const unsigned int audiobufferID, bool repeat = false );
	void SetPosition( const glm::vec3 & pos );
	void SetDirection( const glm::vec3 & forward );

	bool IsPlaying() const;

	void SetSourceRelativePos( bool relative );

    unsigned int GetSourceID() const;
private:
	AudioSource( const AudioSource & other );
	AudioSource & operator = ( const AudioSource & other );

	unsigned int mSourceID;
	unsigned int mLastPlayedBufferID;
	bool mIsRelative;

	glm::vec3 mPos;
	glm::vec3 mForward;

	bool mSourceLoaded;
};

