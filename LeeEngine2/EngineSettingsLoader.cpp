#include "EngineSettingsLoader.h"

#include "StrUtils.h"
#include "Logger.h"

#include <fstream>
using namespace std;

EngineSettingsLoader::EngineSettingsLoader()
	{

	}

EngineSettingsLoader::~EngineSettingsLoader()
	{

	}

bool EngineSettingsLoader::LoadSettings()
	{
	static string filename( "../../LeeEngine2SubDirs/Art/Config/settings.ini" );

	ToPlatformPath( filename );

	ifstream ifs( filename, ios::in );

	if ( !ifs.is_open() )
		{
		LogFailure( "Failed to load settings file " + filename )
		return false;
		}

	string command;

	while ( ifs >> command )
		{
		if ( command == "screenWidth" )
			{
			ifs >> mScreenWidth;
			}
		else if ( command == "screenHeight" )
			{
			ifs >> mScreenHeight;
			}
		else if ( command == "samplesAA" )
			{
			ifs >> mSamplesAA;
			}
		else if ( command == "samplesAF" )
			{
			ifs >> mSamplesAF;
			}
		else
			{
			LogWarning("Unknown command in settings file, " + command )
			}

		if ( ifs.bad() )
			{
			return false;
			}
		}

	return !ifs.bad();
	}

unsigned int EngineSettingsLoader::GetScreenWidth() const
	{
	return mScreenWidth;
	}

unsigned int EngineSettingsLoader::GetScreenHeight() const
	{
	return mScreenHeight;
	}

unsigned int EngineSettingsLoader::GetSamplesAA() const
	{
	return mSamplesAA;
	}

unsigned int EngineSettingsLoader::GetSamplesAF() const
	{
	return mSamplesAF;
	}

