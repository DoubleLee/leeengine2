/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include <string>
using std::string;

// will convert / to \ or \ to / depending on detected platform
// will ensure cross platform support.
string& ToPlatformPath( string & path );

// Will return a new string with the path removed and only the file name remains with extension
// assumes the string already contains proper platform slashes, use ToPlatformPath to make sure
string FileNameFromPath( const string & path );
