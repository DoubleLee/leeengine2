/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"
using namespace glm;

class OrientedBB;

enum class Orientation
	{
	NULLSIDE = 0,
	NEGATIVE_SIDE,
	IN_SIDE,
	POSITIVE_SIDE
	};

// Calculated as 3 points defined counterclockwise
class Plane
{
public:
	Plane();
	Plane(const vec3 & a, const vec3 & b, const vec3 & c);
	~Plane(void);

	// Tests for Plane OBB collision
	bool Collides( const OrientedBB & obb );

	// Returns the Orientation of the Plane and the OBB
	Orientation GetPlaneBoxOrientation( const OrientedBB & obb );

	// static method to create a plane
	static Plane ComputePlane( const vec3 & a, const vec3 & b, const vec3 & c );

	
private:
	// calculate distances from box to plane, center and projected radius
	inline void CalculateDistances( const OrientedBB & obb, float & projectedRadius, float & boxCenterToPlane );

	// normal of the plane
	vec3 mNormal;

	// dot product or squared distance from origin
	float mDot;
};
