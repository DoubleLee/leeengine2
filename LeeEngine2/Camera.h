/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "ViewFrustum.h"

// Holds the camera related data
// will setup itself with default camera settings
// that maybe overridden if you wish.
class Camera
{
public:
	Camera(void);
	~Camera(void);

	// Sets the view matrix
	void SetView( const glm::vec3 & pos, const glm::vec3 & lookAt, const glm::vec3 & up );
	// sets the projection matrix
	void SetProj(float fovy, float aspect, float near, float far);

	const glm::mat4 & GetProjView();
	const glm::mat4 & GetView() const;

	const glm::vec3 & GetPos() const;
	const glm::vec3 & GetLookAt() const;
	const glm::vec3 & GetUp() const;

	const glm::vec3 GetForward() const;

	// makes it possible to do camera * mWorldMatrix;
	operator const glm::mat4 & ();

	// matrix multiply operator
	glm::mat4 operator * ( const glm::mat4 & other );

	ViewFrustum mViewFrustum;

protected:

	glm::mat4 mView;
	glm::mat4 mProj;
	glm::mat4 mProjView;

	glm::vec3 mPos;
	glm::vec3 mLookAt;
	glm::vec3 mUp;
	glm::vec3 mForward;

	float mNearPlane;
	float mFarPlane;
	float mFovy;
	float mAspect;

	bool mNeedsRecalculate;
};

