/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include "Resource.h"

class AudioData : public Resource
{
public:
	AudioData(void);
	virtual ~AudioData(void);

	// Only supports WAVs for now
	virtual bool LoadFromFile( const string & file );
	virtual void ReleaseResources();
	virtual void Bind() const;

    unsigned int GetBufferID() const;

protected:
	
private:
	unsigned int mAudioID;
	
};
