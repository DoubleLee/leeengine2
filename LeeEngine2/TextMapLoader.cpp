#include "TextMapLoader.h"

#include "Logger.h"
#include "Engine.h"
#include "ResourceManager.h"
#include "SafeEntity.h"
#include "MeshData.h"
#include "TextureData.h"
#include "ShaderProgram.h"
#include "Skybox.h"
#include "StrUtils.h"
#include "MathUtilities.h"
#include "AudioManager.h"
#include "AudioData.h"

#include <fstream>
#include <string>

using namespace std;

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

TextMapLoader::TextMapLoader()
	:
	mpEngine(nullptr),
	mIsLoaded(false)
	{

	}

TextMapLoader::~TextMapLoader()
	{

	}

bool TextMapLoader::LoadMap(Engine * pEngine, string file)
	{
	LogFuncBegin()

	if ( pEngine == nullptr )
		return false;

	mpEngine = pEngine;

	ToPlatformPath(file);

	ifstream ifs( file, std::ios::in );

	if ( !ifs.is_open() )
		{
		LogFailure("Failed to open map file " + file )
		return mIsLoaded = false;
		}

	string command;
	string data;

	while ( ifs >> command )
		{
		FindResult result = FindVirtualCommand( command, data, ifs );
		if ( result == FindResult::ResultFound )
			{
			// TODO: maybe something needs to happen when command is found.
			}
		else if ( result == FindResult::ResultNotFound )
			{
			LogWarning("Unknown command found " + command)
			}
		else if ( result == FindResult::ResultError )
			{
			LogFailure("Error while loading command " + command)
			return false;
			}
		if ( ifs.bad() || ifs.fail() )
			return false;
		}

	LogFuncEndSuccess()
	return true;
	}

FindResult TextMapLoader::FindVirtualCommand(string & command, string & data, ifstream & ifs)
	{
	if ( command[0] == '#' )
		{
		std::getline(ifs, data); // found a # comment line
		return FindResult::ResultFound;
		}
	else if ( command == "mesh")// [mesh fileName] // no spaces in file name
		{
		// get file name
		ifs >> data;
		SharedPointer<MeshData> pMesh = mpEngine->mpMeshMan->GetOrLoadFromFile( ToPlatformPath(data) );

		if ( !pMesh )
			{
			LogFailure("Failed to load mesh command's data " + data)
			return FindResult::ResultError;
			}
		else
			{
			mMeshes.push_back(pMesh);
			return FindResult::ResultFound;
			}
		}
	else if ( command == "texture" )// [texture fileName] // no spaces in file name
		{
		// get file name
		ifs >> data;
		SharedPointer<TextureData> pTex = mpEngine->mpTexMan->GetOrLoadFromFile( ToPlatformPath(data) );
		if ( !pTex )
			{
			LogFailure("Failed to load texture command's data " + data)
			return FindResult::ResultError;
			}
		else
			{
			mTextures.push_back(pTex);
			return FindResult::ResultFound;
			}
		}
	else if ( command == "program")// [shader vertexShaderFileName fragmentShaderFileName]
		{
		// TODO: make this support multiple programs by creating resource manager for shaders
		string vertFile;
		string fragFile;
		ifs >> vertFile;
		ifs >> fragFile;

		SharedPointer<ShaderProgram> pProgram = mpEngine->mpProgMan->GetOrLoadFromFiles(ToPlatformPath(vertFile),ToPlatformPath(fragFile));

		if ( !pProgram )
			{
			LogFailure("Unable to load shader from map file " + vertFile += ' ' + fragFile)
			return FindResult::ResultError;
			}
		else
			{
			mPrograms.push_back(pProgram);
			return FindResult::ResultFound;
			}
		}
	else if ( command == "audioEffect" )
		{
		unsigned int nameID;
		ifs >> nameID;

		ifs >> data;

		if ( ifs.bad() )
			{
			return FindResult::ResultError;
			}

		AudioData * pData( new AudioData() );
		if ( !pData->LoadFromFile( data ) )
			{
			LogFailure("Failed to load audio file " + data )
			delete pData;
			return FindResult::ResultError;
			}

		if ( !mpEngine->mpAudioMan->AddRegisteredAudio( nameID, pData ) )
			{
			delete pData;
			return FindResult::ResultError;
			}
		return FindResult::ResultFound;
		}
	else if ( command == "entity" )
		{
		unsigned int meshIndex;
		ifs >> meshIndex;
		unsigned int textureIndex;
		ifs >> textureIndex;
		unsigned int programsIndex;
		ifs >> programsIndex;
		glm::vec3 pos;
		ifs >> pos.x;
		ifs >> pos.y;
		ifs >> pos.z;

		if ( ifs.bad() )
			{
			return FindResult::ResultError;
			}

		SafeEntity * pEnt = new SafeEntity();

		pEnt->Init();
		pEnt->mWorld = glm::translate( mat4identity, pos );
		pEnt->SetMesh( mMeshes[meshIndex] );
		pEnt->SetTexture( mTextures[textureIndex] );
		pEnt->SetProgram(  mPrograms[programsIndex] );

		mpEngine->mEntities.emplace_back( pEnt );
		return FindResult::ResultFound;
		}
	else if ( command == "skybox" )
		{
		mpEngine->mpSkybox.reset( new Skybox() );

		unsigned int meshIndex;
		ifs >> meshIndex;
		unsigned int textureIndex;
		ifs >> textureIndex;
		unsigned int programsIndex;
		ifs >> programsIndex;

		if ( ifs.bad() )
			{
			return FindResult::ResultError;
			}

		mpEngine->mpSkybox->SetMesh( mMeshes[meshIndex] );
		mpEngine->mpSkybox->SetTexture( mTextures[textureIndex] );
		mpEngine->mpSkybox->SetProgram( mPrograms[programsIndex] );
		return FindResult::ResultFound;
		}
	else
		{
		return FindResult::ResultNotFound;
		}
	}
