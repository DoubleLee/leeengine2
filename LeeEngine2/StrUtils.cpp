/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "StrUtils.h"

string & ToPlatformPath( string & path )
	{
	char LOOKFOR;
	char REPLACEWITH;
	// TODO: add more platforms here
	#ifdef WIN32
		LOOKFOR = '/';
		REPLACEWITH = '\\';
	#else
		LOOKFOR = '\\';
		REPLACEWITH = '/';
	#endif

	if ( path.empty() ) 
		return path;
	
	for ( unsigned int i = 0; i < path.size(); ++i )
		{
		if ( path[i] == LOOKFOR )
			path[i] = REPLACEWITH;
		}

	return path;
	}

std::string FileNameFromPath( const std::string & fileName)
	{
	char LOOKFOR;
	// TODO: add more platforms here
	#ifdef WIN32
		LOOKFOR = '\\';
	#else
		LOOKFOR = '/';
	#endif
    string::size_type indexLastBackSlash = fileName.find_last_of(LOOKFOR);

	if ( indexLastBackSlash != string::npos )
		{
		++indexLastBackSlash; // move past last LOOKFOR character
		if ( indexLastBackSlash < fileName.size() )
			{
			return fileName.substr(indexLastBackSlash);
			}
		else
			{
			return fileName.substr(--indexLastBackSlash);
			}
		}

	return fileName;
	}
