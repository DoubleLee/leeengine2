/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include "Resource.h"

class TextureData : public Resource
{
public:
	TextureData();
	virtual ~TextureData();

	// loads a texture from file, uses freeimage so it can load all major file types
	virtual bool LoadFromFile( const std::string & file );
	// releases the resources immediately
	virtual void ReleaseResources();
	// binds the texture to the renderer
	virtual void Bind() const;

	unsigned int mTextureID;
	int mWidth;
	int mHeight;
	int mBitsPerPixel;

protected:

private:
	TextureData( const TextureData & other );
	TextureData & operator = ( const TextureData & other );
};
