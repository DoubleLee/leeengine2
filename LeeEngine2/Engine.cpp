/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Engine.h"
#include "Logger.h"
#include "RandGen.h"
#include "Camera.h"
#include "ResourceManager.h"
#include "MeshData.h"
#include "TextureData.h"
#include "ShaderProgram.h"
#include "Skybox.h"
#include "MathUtilities.h"
#include "AudioManager.h"
#include "EngineSettingsLoader.h"
#include "Window.h"

#include "GL/glew.h"

#include "GLFW/glfw3.h"

#include "AL/alut.h"
#include "AL/al.h"

// just in case you want to run this as a Win32 Windows style application ( not console )
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
int main();
int CALLBACK WinMain(_In_  HINSTANCE hInstance, _In_  HINSTANCE hPrevInstance, _In_  LPSTR lpCmdLine, _In_  int nCmdShow)
	{
	return main();
	}
#endif

Engine::Engine()
	:
	mpAudioMan(new AudioManager() ),
	mpMeshMan( new ResourceManager<MeshData> () ),
	mpTexMan( new ResourceManager<TextureData> () ),
	mpProgMan( new ResourceManagerTwoFiles<ShaderProgram> () ),
	mpSkybox(),
	mEntities(),
	mpCamera( new Camera() ),
	mWindowTitle(),
	mReturnVal(0),
	mFrameStampD(0.0),
	mFrameDeltaD(0.0),
	mFrameStampF(0.0f),
	mFrameDeltaF(0.0f),
	mFramesPerSec(0),
	mpWindow(nullptr)
	{

	}

Engine::~Engine()
	{
	mEntities.clear();
	mpSkybox.reset();
	mpMeshMan.reset();
	mpTexMan.reset();
	mpProgMan.reset();
	mpAudioMan.reset();
	mpCamera.reset(); // TODO: not really necessary but it felt right to reset it aswell

	ReleaseAllApis();
	}

bool Engine::Init(int width, int height)
	{
	LogFuncBegin()

	EngineSettingsLoader settings;
	settings.LoadSettings();

	if ( !settings.LoadSettings() )
		{
		LogFailure("Engine settings failed to load.")
		return false;
		}

	if ( !SetupWindowAndContext( &settings ) || !SetupOpenGLFunctionPointers() || !SetupOpenALAndALUT() )
		{
		LogFailure("Engine::Init() failed to setup engine.")
		return false;
		}

	// Seeds random generation engines
	// based on time using chrono
	Rand::SeedRandomEngines();

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	//glFrontFace( GL_CW );
	glCullFace(GL_BACK);

	mpCamera->SetView(vec3(0.0f, 50.0f, 0.0f), vec3zero, vec3front );
	mpCamera->SetProj( 3.14f / 2.0f, float( settings.GetScreenWidth() ) / float(settings.GetScreenHeight() ), 1.0f, 1000.0f );

	RegisterAudioEngineEnums();
	RegisterAudioGameEnums();

	LogFuncEndSuccess()
	return true;
	}

int Engine::Run()
	{
	mFrameStampD = glfwGetTime();
	mFrameStampF = float(mFrameStampD);
	while( !mpWindow->ShouldClose() )
		{
		glfwPollEvents();
		mpAudioMan->Update();

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );


		// calculate time.
		double now ( glfwGetTime() );
		mFrameDeltaD = now - mFrameStampD;
		mFrameStampD = now;

		mFrameDeltaF = float(mFrameDeltaD);
		mFrameStampF = float(mFrameStampD);

		// count the fps
		static float fpsTrigger(0.0f);
		static int fps(0);
		if ( fpsTrigger < mFrameStampF )
			{
			mFramesPerSec = fps;
			fps = 0;
			fpsTrigger = mFrameStampF + 1.0f;
			}
		++fps;

		GameUpdate();
		GameDraw();

		mpWindow->SetWindowTitle( mWindowTitle );
		mWindowTitle.clear();

		mpWindow->SwapBuffers();
		}
	return mReturnVal;
	}

bool Engine::SetupWindowAndContext(const EngineSettingsLoader * pSettings)
	{
	mpWindow.reset( new Window() );

	return mpWindow->Init(pSettings);
	}

bool Engine::SetupOpenGLFunctionPointers()
	{
	LogFuncBegin()
	glewExperimental = true;

	if ( glewInit() != GLEW_OK )
		{
		LogFailure("glewInit() returned error")
		return false;
		}

	// TODO: Maybe someday this bit of code won't be necessary
	// but for now we need to clear an openGL error.
	GLenum error = glGetError();
	if ( error != GL_NO_ERROR )
		{
		LogWarning("glew still producing error after glewInit with glewExperimental = true")
		}
	LogFuncEndSuccess()
	return true;
	}

bool Engine::SetupOpenALAndALUT()
	{
	LogFuncBegin()
	if ( !alutInit( NULL, NULL ) )
		{
		LogFailure("alutInit() failed.")
		return false;
		}

	// wipe any errors alutInit may have called.
	ALenum result = alGetError();
	if ( result != AL_NO_ERROR )
		{
		LogWarning("Alut produced an error during init but was ignored.")
		}

	alListenerf( AL_GAIN, 1.0f );
	alListener3f( AL_POSITION, 0.0f, 0.0f, 0.0f );
	alListener3f( AL_VELOCITY, 0.0f, 0.0f, 0.0f );
	//float forwardAndUp[] = { 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f };
	//alListenerfv( AL_ORIENTATION, forwardAndUp );

	result = alGetError();
	if (result != AL_NO_ERROR)
		{
		LogError("openAL returned error after setting up default listener.", alutGetErrorString(result), result )
		return false;
		}

	LogFuncEndSuccess()
	return true;
	}

void Engine::RegisterAudioEngineEnums()
	{
	for ( unsigned int i = 0; i < Registered::LastAudioEngine; ++i )
		{
		mpAudioMan->AddRegisteredName( i );
		}
	}

void Engine::RegisterAudioGameEnums()
	{

	}

void Engine::ReleaseAllApis()
	{
	ReleaseWindowAndContext();
	ReleaseOpenAL();
	}

void Engine::ReleaseWindowAndContext()
	{
	glfwTerminate();
	}

void Engine::ReleaseOpenAL()
	{
	alutExit();
	}

Window * Engine::GetWindow() const
	{
	return mpWindow.get();
	}
