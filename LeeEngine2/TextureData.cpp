/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "TextureData.h"

#include "FreeImage.h"

#include "Logger.h"

#include "GL/glew.h"

TextureData::TextureData()
	:
	mTextureID(0),
	mWidth(0),
	mHeight(0),
	mBitsPerPixel(0)
	{

	}


TextureData::~TextureData()
	{
	ReleaseResources();
	}

bool TextureData::LoadFromFile( const std::string & file )
	{
	ReleaseResources();

	FREE_IMAGE_FORMAT format = FreeImage_GetFileType( file.c_str(), 0);

	FIBITMAP * pImageData = FreeImage_Load( format, file.c_str() );

	if ( !pImageData )
		{
		LogFailure("Failed to load texture " + file)
		return (mIsLoaded = false);
		}

	FIBITMAP * pConvertedImageData = FreeImage_ConvertTo32Bits(pImageData);
	if ( !pConvertedImageData )
		{
		FreeImage_Unload( pImageData );

		LogFailure("Failed while converting a loaded texture " + file )
		return (mIsLoaded = false);
		}

	int width = FreeImage_GetWidth( pConvertedImageData );
	int height = FreeImage_GetHeight( pConvertedImageData );
	int bitsPerPixel = FreeImage_GetBPP( pConvertedImageData );
	char * pImageBytes = (char*)FreeImage_GetBits( pConvertedImageData );

	glGenTextures(1, &mTextureID);
	glBindTexture( GL_TEXTURE_2D, mTextureID );

	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, pImageBytes );

	//glTexParameterf( GL_TEXTURE_2D, GL_EXT_texture_filter_anisotropic, 4.0 );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );


	glBindTexture( GL_TEXTURE_2D, 0 );

	GLenum error = glGetError();

	if ( error != GL_NO_ERROR )
		{
		LogError("Error while sending openGL texture data " + file, (char*)glewGetErrorString(error), error)
		if ( mTextureID )
			{
			glDeleteTextures(1, &mTextureID);
			mTextureID = 0;
			}
		FreeImage_Unload(pImageData);
		FreeImage_Unload(pConvertedImageData);
		return (mIsLoaded = false);
		}

	FreeImage_Unload(pImageData);
	FreeImage_Unload(pConvertedImageData);

	mWidth = width;
	mHeight = height;
	mBitsPerPixel = bitsPerPixel;

	mFileName = file;
	LogSuccess("Image loaded " + file)
	return (mIsLoaded = true);
	}

void TextureData::Bind() const
	{
	glBindTexture(GL_TEXTURE_2D, mTextureID);
	}

void TextureData::ReleaseResources()
	{
	glBindTexture(GL_TEXTURE_2D , 0);
	if ( mTextureID )
		{
		glDeleteTextures(1, &mTextureID);
		mTextureID = 0;
		}

	mWidth = 0;
	mHeight = 0;
	mBitsPerPixel = 0;

	mIsLoaded = false;
	}
