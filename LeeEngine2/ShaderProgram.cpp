/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "ShaderProgram.h"

#include <fstream>

#include "Logger.h"

#include "GL/glew.h"

ShaderProgram::ShaderProgram()
	:
	mVertID(0u),
	mFragID(0u),
	mProgramID(0u),
	mProgramLoaded(false)
	{

	}

ShaderProgram::~ShaderProgram()
	{
	ReleaseResources();
	}

bool ShaderProgram::LoadFromFiles( const string & vertFilePath, const string & fragFilePath )
	{
	LogMessage("LoadShader called")
	ReleaseResources();
	mVertID = glCreateShader(GL_VERTEX_SHADER);
	mFragID = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertCode;
	std::ifstream fileStream(vertFilePath, std::ios::in | std::ios::ate);

	if (!fileStream.is_open() || !fileStream.good())
		{
		LogFailure("Failed to load vertex shader file")
		return (mProgramLoaded = false);
		}

	vertCode.resize(int(fileStream.tellg()), 0);

	fileStream.seekg(0);
	fileStream.read(&vertCode[0], vertCode.size());

	if (fileStream.bad())
		{
		LogFailure("Failed while loading vertex shader code")
		fileStream.close();
		return (mProgramLoaded = false);
		}

	fileStream.close();
	fileStream.clear();
	std::string fragCode;
	fileStream.open(fragFilePath, std::ios::in | std::ios::ate);

	if (!fileStream.is_open() || !fileStream.good())
		{
		LogFailure("Failed to load fragment shader file")
		return (mProgramLoaded = false);
		}

	fragCode.resize(int(fileStream.tellg()), 0);

	fileStream.seekg(0);
	fileStream.read(&fragCode[0], fragCode.size());

	if (fileStream.bad())
		{
		LogFailure("Failed while loading fragment shader code")
		fileStream.close();
		return (mProgramLoaded = false);
		}

	fileStream.close();
	LogSuccess("Pulling code from shaders success")
	LogMessage("Beginning the openGL shader compile process")

	GLint result = GL_FALSE;

	int infoLogLength = 0;

	LogMessage("Compiling vert shader")
	const char * vertSourcePointer = vertCode.data();
	int vertSourceSize = vertCode.size() + 1;
	glShaderSource(mVertID, 1, &vertSourcePointer, &vertSourceSize);
	glCompileShader(mVertID);

	GLenum errorCode = glGetError();

	if (errorCode != GL_NO_ERROR)
		{
		LogError("Error while compiling vert shader", (char*) glewGetErrorString(errorCode), errorCode)
		return (mProgramLoaded = false);
		}

	// check vert shader for errors
	glGetShaderiv(mVertID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(mVertID, GL_INFO_LOG_LENGTH, &infoLogLength);
	string errorBuffer;
	errorBuffer.resize(infoLogLength + 1, 0);
	glGetShaderInfoLog(mVertID, infoLogLength, NULL, &errorBuffer[0]);

	if (result == GL_FALSE)
		{
		LogError("Error compiling vert shader", errorBuffer, result)
		return (mProgramLoaded = false);
		}

	LogSuccess("vert shader compilation success")

	LogMessage("Compiling frag shader")

	char const * fragSourcePointer = fragCode.data();
	int fragSize = fragCode.size() + 1;
	glShaderSource(mFragID, 1, &fragSourcePointer, &fragSize);
	glCompileShader(mFragID);

	errorCode = glGetError();

	if (errorCode != GL_NO_ERROR)
		{
		LogError("Error while compiling vert shader", (char*) glewGetErrorString(errorCode), errorCode)
		return (mProgramLoaded = false);
		}

	result = GL_FALSE;
	infoLogLength = 0;

	// check frag shader for errors
	glGetShaderiv(mFragID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(mFragID, GL_INFO_LOG_LENGTH, &infoLogLength);
	errorBuffer.clear();
	errorBuffer.resize(infoLogLength + 1, 0);
	glGetShaderInfoLog(mFragID, infoLogLength, NULL, &errorBuffer[0]);

	if (result == GL_FALSE)
		{
		LogError("Error while compiling frag shader", errorBuffer, result)
		return (mProgramLoaded = false);
		}

	LogSuccess("frag shader compilation success")
	LogSuccess("Vert and Frag shaders compilation successfull")

	// link shaders into program
	LogMessage("Beginning program link")
	mProgramID = glCreateProgram();
	glAttachShader(mProgramID, mVertID);
	glAttachShader(mProgramID, mFragID);
	glLinkProgram(mProgramID);

	result = GL_FALSE;
	infoLogLength = 0;
	// check program
	glGetProgramiv(mProgramID, GL_LINK_STATUS, &result);
	glGetProgramiv(mProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);
	errorBuffer.clear();
	errorBuffer.resize(infoLogLength + 1);
	glGetProgramInfoLog(mProgramID, infoLogLength, NULL, &errorBuffer[0]);

	if (result == GL_FALSE)
		{
		LogError("Error Linking vert and frag shaders into program", errorBuffer, result)
		return (mProgramLoaded = false);
		}

	// may consider not tagging shaders for deletion,
	// I heard this can cause problems on openGL ES
	glDeleteShader(mVertID);
	glDeleteShader(mFragID);

	LogSuccess("Shaders loaded " + vertFilePath + ' ' + fragFilePath )
	mVertFile = vertFilePath;
	mFragFile = fragFilePath;

	return (mProgramLoaded = true);
	}

void ShaderProgram::Bind()
	{
	glUseProgram(mProgramID);
	}

void ShaderProgram::ReleaseResources()
	{
	if ( mVertID )
		{
		glDeleteShader(mVertID);
		mVertID = 0;
		}

	if ( mFragID )
		{
		glDeleteShader(mFragID);
		mFragID = 0;
		}

	if ( mProgramID )
		{
		glDeleteProgram(mProgramID);
		mProgramID = 0;
		}

	mVertFile.clear();
	mFragFile.clear();
	mProgramLoaded = false;
	}


void ShaderProgram::SetUniform(const char * pUniformName, const glm::mat4 & mat)
	{
	GLint loc = glGetUniformLocation( mProgramID, pUniformName );

	if ( 0 <= loc )
		{
		glProgramUniformMatrix4fv(mProgramID, loc, 1, GL_FALSE, &mat[0][0]);
		}

	}

void ShaderProgram::SetUniform(const char * pUniformName, const glm::vec3 & vec3)
	{
	GLint loc = glGetUniformLocation( mProgramID, pUniformName );

	if ( 0 <= loc )
		{
		glProgramUniform3fv(mProgramID, loc, 1, &vec3[0] );
		}
	}

void ShaderProgram::SetUniform(const char * pUniformName, const float f)
	{
	GLint loc = glGetUniformLocation(mProgramID, pUniformName);

	if (0 <= loc)
		{
		glProgramUniform1f(mProgramID, loc, f);
		}
	}

void ShaderProgram::SetUniform(const char * pUniformName, const int i)
	{
	GLint loc = glGetUniformLocation(mProgramID, pUniformName);

	if (0 <= loc)
		{
		glProgramUniform1i(mProgramID, loc, i);
		}
	}

void ShaderProgram::SetUniform(const char * pUniformName, const bool b)
	{
	GLint loc = glGetUniformLocation(mProgramID, pUniformName);

	if (0 <= loc)
		{
		glProgramUniform1ui(mProgramID, loc, b ? GL_TRUE : GL_FALSE);
		}
	}

bool ShaderProgram::IsLoaded() const
	{
	return mProgramLoaded;
	}

unsigned int ShaderProgram::GetProgramID() const
	{
	return mProgramID;
	}
