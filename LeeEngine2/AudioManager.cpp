/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "AudioManager.h"

#include "Logger.h"
#include "AudioData.h"
#include "AudioSource.h"
#include "AudioListener.h"
#include "RandGen.h"

#include "glm/glm.hpp"
using namespace glm;
#include "AL/al.h"

#include <algorithm>

RegisteredAudio::RegisteredAudio(int soundID)
	:
	mAudioID( Registered::AudioEngineEnum(soundID) ),
	mRelatedAudioFiles()
	{

	}

bool RegisteredAudio::operator == ( const RegisteredAudio & other )
	{
	return mAudioID == other.mAudioID;
	}

AudioManager::AudioManager()
	:
	mpListener( new AudioListener() ),
	mRegisteredAudio(),
	mQuickPlaySources()
	{

	}

AudioManager::~AudioManager()
	{

	}

void AudioManager::AddRegisteredName( int nameID )
	{
	mRegisteredAudio.emplace_back(nameID);
	}

bool AudioManager::AddRegisteredAudio( int audioID, AudioData * pAudioData )
	{
	auto iter = std::find( mRegisteredAudio.begin(), mRegisteredAudio.end(), audioID );

	if ( iter != mRegisteredAudio.end() )
		{
		(*iter).mRelatedAudioFiles.emplace_back( pAudioData );
		return true;
		}
	else
		{
		return false;
		}
	}

void AudioManager::PlayRegisteredAudio( int audioID )
	{
	PlayRegisteredAudio( audioID, vec3(0.0f), true );
	}

void AudioManager::PlayRegisteredAudio( int audioID, const vec3 & pos, bool relative)
	{
	auto iter = std::find( mRegisteredAudio.begin(), mRegisteredAudio.end(), audioID );
	if ( iter != mRegisteredAudio.end() )
		{
		RegisteredAudio & reg = (*iter);
		if ( !reg.mRelatedAudioFiles.empty() )
			{
			AudioSource * pSource = new AudioSource();
			UniquePointer<AudioSource> upSource( pSource );

			pSource->LoadSource();
			pSource->SetPosition( pos );
			pSource->SetSourceRelativePos( relative );
			// plays random audio from entry
			pSource->Play(reg.mRelatedAudioFiles.at( Rand::GetRandomUnsignedInt(0, reg.mRelatedAudioFiles.size() - 1 ))->GetBufferID(), false );
			mQuickPlaySources.push_back( std::move(upSource) );
			}
		else
			{
			LogWarning("A requested registered audio entry was found, but no audios were registered to that entry.")
			}
		}
	else
		{
		LogWarning("A requested registered audio entry was not found.")
		}
	}

void AudioManager::Update()
	{
	for ( auto iter = mQuickPlaySources.begin(); iter != mQuickPlaySources.end(); )
		{
		AudioSource * pSource = (*iter).get();
		if ( !pSource->IsPlaying() )
			{
			iter = mQuickPlaySources.erase(iter);
			}
		else
			{
			iter++;
			}
		}

	}

void AudioManager::SetListenerPos(const vec3 & pos)
	{
	mpListener->SetPosition( pos );
	}

void AudioManager::SetListenerOrient(const vec3 & front, const vec3 & up)
	{
	mpListener->SetOrientation( front, up );
	}

void AudioManager::SetListenerPosAndOrient(const vec3 & pos, const vec3 & front, const vec3 & up)
	{
	mpListener->SetPosition( pos );
	mpListener->SetOrientation( front, up );
	}

