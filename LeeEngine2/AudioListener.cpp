/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "AudioListener.h"

#include "Logger.h"

#include "AL/al.h"
#include "AL/alut.h"

#include <array>
#include <exception>
using namespace std;

bool AudioListener::mSingletonListenerCreated = false;

AudioListener::AudioListener()
	:
	mGainVolume(1.0f),
	mPos(0.0f),
	mVel(0.0f),
	mForward(0.0f, 0.0f, -1.0f),
	mUp( 0.0f, 1.0f, 0.0f )
	{
	if ( mSingletonListenerCreated )
		{
		LogFailure("Attempted to create second audio listener")
		throw exception();
		}
	mSingletonListenerCreated = true;

	SetMasterVolume(mGainVolume);
	SetPosition(mPos);
	SetVelocity(mVel);
	SetOrientation(mForward, mUp);

	ALenum result = alGetError();
	if( result != AL_NO_ERROR )
		{
		LogError("Error while setting up default listener settings", alutGetErrorString(result), result)
		}
	}


AudioListener::~AudioListener()
	{
	mSingletonListenerCreated = false;
	}

void AudioListener::SetMasterVolume( const float volume)
	{
	alListenerf( AL_GAIN, volume);
	mGainVolume = volume;
	}

void AudioListener::SetPosition( const vec3 & pos )
	{
	alListenerfv(AL_POSITION, &pos[0]);
	mPos = pos;
	}

void AudioListener::SetVelocity( const vec3 & vel )
	{
	alListenerfv(AL_VELOCITY, &vel[0]);
	mVel = vel;
	}

void AudioListener::SetOrientation( const vec3 & forward, const vec3 & up )
	{
	array<float, 6> orient = { {forward.x, forward.y,forward.z, up.x, up.y, up.z} };
	alListenerfv( AL_ORIENTATION, orient.data() );
	mForward = forward;
	mUp = up;
	}

float AudioListener::GetMasterVolume() const
	{
	return mGainVolume;
	}

vec3 AudioListener::GetPosition() const
	{
	return mPos;
	}

vec3 AudioListener::GetVelocity() const
	{
	return mVel;
	}

vec3 AudioListener::GetForward() const
	{
	return mForward;
	}

vec3 AudioListener::GetUp() const
	{
	return mUp;
	}

