/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "MathUtilities.h"

const float ZEROF(0.0f);
const double ZEROD(0.0);

const float ONEF(1.0f);
const double ONED(1.0);

const vec3 vec3up(0.0f, 1.0f, 0.0f);
const vec3 vec3down(0.0f, -1.0f, 0.0f);
const vec3 vec3left(-1.0f, 0.0f, 0.0f);
const vec3 vec3right(1.0f, 0.0f, 0.0f);

const vec3 vec3front(0.0f, 0.0f, -1.0f);
const vec3 vec3back(0.0f, 0.0f, 1.0f);

const vec3 vec3zero(0.0f);
const vec3 vec3one(1.0f);

extern const mat4 mat4identity(1.0f);
