/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#include "Plane.h"


#include "glm/glm.hpp"
using namespace glm;

#include <array>
using std::array;

class OrientedBB;

class ViewFrustum
{
public:
	ViewFrustum(void);
	ViewFrustum(const vec3 & p, const vec3 & d, const vec3 & up, const float fov, const float ratio, const float nearDist, const float farDist );
	~ViewFrustum(void);

	enum Planes
		{
		TOP = 0,
		RIGHT,
		BOTTOM,
		LEFT,
		NEAR,
		FAR,
		};

	// takes in a obb and checks against the view frustum if the object inside should be culled
	bool ShouldBeCulled( const OrientedBB & obb );

	// returns all the orientations for an obb against the 6 planes of the view frustum
	array<Orientation, 6> GetPlaneOrientationsForOBB( const OrientedBB & obb );
	
	array<Plane, 6> mPlanes;
};

