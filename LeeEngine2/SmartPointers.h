/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#ifndef SHAREDPOINTER_H
#define SHAREDPOINTER_H

#include <memory>

template<typename T> using SharedPointer = std::shared_ptr<T>;
template<typename T> using WeakPointer = std::weak_ptr<T>;
template<typename T> using UniquePointer = std::unique_ptr<T>;

#endif // SHAREDPOINTER_H
