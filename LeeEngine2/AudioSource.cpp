/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "AudioSource.h"

#include "Logger.h"

#include "AL/al.h"
#include "AL/alut.h"

#include <iostream>
#include <array>
using namespace std;

AudioSource::AudioSource(void)
	:
	mSourceID(0),
	mLastPlayedBufferID(0),
	mIsRelative(false),
	mPos(0.0f),
	mForward(0.0f, 0.0f, -1.0f),
	mSourceLoaded(false)
	{

	}


AudioSource::~AudioSource(void)
	{
	ReleaseResources();
	}

bool AudioSource::LoadSource()
	{
	ReleaseResources();
	alGenSources(1, &mSourceID);

	if ( !mSourceID )
		{
		LogFailure("Failed to generate an al source")
		return (mSourceLoaded = false);
		}

	SetPosition(mPos);
	alSourcef( mSourceID, AL_PITCH, 1.0f );
	alSourcef( mSourceID, AL_GAIN, 1.0f );
	SetSourceRelativePos(false);

	ALenum result = alGetError();

	if ( result != AL_NO_ERROR )
		{
		LogError("al returned error while loaded source", alutGetErrorString(result), result)
		return (mSourceLoaded = false);
		}

	return (mSourceLoaded = true);
	}

void AudioSource::ReleaseResources()
	{
	if ( mSourceID )
		{
		alSourceStop( mSourceID );
		alDeleteSources(1, &mSourceID );
		mSourceID = 0;
		}

	mSourceLoaded = false;
	}

void AudioSource::Play( const unsigned int audioBufferID, const bool repeat )
	{
	if ( mSourceID )
		{
		mLastPlayedBufferID = audioBufferID;
		alSourcei( mSourceID, AL_BUFFER, audioBufferID );

		int playing = 0;

		alGetSourcei(mSourceID, AL_SOURCE_STATE, &playing);

		if ( playing == AL_PLAYING )
			{
			return;
			}

		alSourcei( mSourceID, AL_LOOPING, repeat ?  AL_TRUE : AL_FALSE );

		alSourcePlay( mSourceID );

		ALenum result = alGetError();
		if ( result != AL_NO_ERROR )
			{
			LogError("failed while playing source", alutGetErrorString(result), result)
			return;
			}
		}
	}

void AudioSource::SetPosition( const glm::vec3 & pos )
	{
	if ( mSourceID )
		{
		alSource3f( mSourceID, AL_POSITION, pos.x, pos.y, pos.z );
		mPos = pos;
		}
	}

void AudioSource::SetDirection( const glm::vec3 & forward )
	{
	if ( mSourceID )
		{
		//array<float, 6> orient = {forward.x, forward.y, forward.z, up.x, up.y, up.z};
		alSource3f( mSourceID, AL_DIRECTION, forward.x, forward.y, forward.z);
		mForward = forward;
		}
	}

bool AudioSource::IsPlaying() const
	{
	ALint state;
	alGetSourcei( mSourceID, AL_SOURCE_STATE, &state );
	return state == AL_PLAYING;// || state == AL_INITIAL;
	}

void AudioSource::SetSourceRelativePos( bool relative )
	{
	if ( mSourceID )
		{
		alSourcei( mSourceID, AL_SOURCE_RELATIVE, relative ? AL_TRUE : AL_FALSE );
		mIsRelative = relative;
		}
	}

unsigned int AudioSource::GetSourceID() const
	{
	return mSourceID;
	}
