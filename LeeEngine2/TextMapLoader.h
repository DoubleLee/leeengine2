#pragma once

#ifndef TEXTMAPLOADER_H
#define TEXTMAPLOADER_H

#include <string>
#include <vector>
#include <iosfwd>
using std::ifstream;
using std::string;
using std::vector;

#include "SmartPointers.h"

class Engine;

class SafeEntity;
class MeshData;
class TextureData;
class ShaderProgram;

enum class FindResult
	{
	ResultError = 0,
	ResultFound,
	ResultNotFound
	};

class TextMapLoader
{
public:
	TextMapLoader();
	virtual ~TextMapLoader();

	// Loads the text file map, takes a pointer to the engine and the file to load
	bool LoadMap(Engine * pEngine, string file);

	// if you inherit from this class or a child of this class
	// be sure you check for your commands and if not found
	// send it on down to the inherited class base
	virtual FindResult FindVirtualCommand(string & command, string & data, ifstream & ifs);

	vector< SharedPointer<MeshData> > mMeshes;
	vector< SharedPointer<TextureData> > mTextures;
	vector< SharedPointer<ShaderProgram> > mPrograms;

protected:
	Engine * mpEngine;
private:
	bool mIsLoaded;
};

#endif // TEXTMAPLOADER_H
