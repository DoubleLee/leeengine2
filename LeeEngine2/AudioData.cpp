/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "AudioData.h"

#include "Logger.h"

#include "AL/alut.h"

AudioData::AudioData(void)
	:
	mAudioID(0)
	{

	}

AudioData::~AudioData(void)
	{
	ReleaseResources();
	}

bool AudioData::LoadFromFile( const string & file )
	{
	ReleaseResources();
	mAudioID = alutCreateBufferFromFile( file.c_str() );

	if ( !mAudioID  )
		{
		LogFailure("Failed to load audio file" + file );
		return (mIsLoaded = false);
		}

	ALenum result = alGetError();

	if( result != AL_NO_ERROR )	
		{
		LogError("openAL returned an Error while loading buffer " + file, alutGetErrorString(result), result )
		return (mIsLoaded = false);
		}

	LogSuccess("Audio file loaded " + file)
	mFileName = file;
	return (mIsLoaded = true);
	}

void AudioData::ReleaseResources()
	{
	if ( mAudioID )
		{
		alDeleteBuffers(1,&mAudioID);
		mAudioID = 0;
		}

	mIsLoaded = false;
	}

void AudioData::Bind() const
	{
	LogMessage("Attempted to Bind AudioData, openAL doesn't use bind.")
	}

unsigned int AudioData::GetBufferID() const
	{
	return mAudioID;
	}
