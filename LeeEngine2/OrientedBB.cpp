/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "OrientedBB.h"


OrientedBB::OrientedBB(const vec3 & pos, const mat3 & rots, const vec3 & extents)
	:
	mPos(pos),
	mMatRots(rots),
	mExtents(extents)
	{

	}


OrientedBB::~OrientedBB()
	{

	}

bool OrientedBB::IsColliding( const OrientedBB & other )
	{
	return IsColliding( this->mPos, this->mMatRots, this->mExtents, other.mPos, other.mMatRots, other.mExtents );
	}

bool OrientedBB::IsColliding( const OrientedBB & first, const OrientedBB & second )
	{
	return IsColliding( first.mPos, first.mMatRots, first.mExtents, second.mPos, second.mMatRots, second.mExtents );
	} 

bool OrientedBB::IsColliding( const vec3 & posA, const mat3 & rotsA, const vec3 & extentsA,
						 const vec3 & posB, const mat3 & rotsB, const vec3 & extentsB )
	{
	float ra, rb;
	mat3 R, Absr;

	// compute rotation matrix expressing b in a's coordinate frame
	for ( int i = 0; i < 3; ++i )
		{
		for ( int j = 0; j < 3; ++j )
			{
			R[i][j] = dot(rotsA[i], rotsB[j]);
			}
		}

	// compute translation vector t
	vec3 t = posB - posA;
	t.x += FLT_EPSILON;
	t.y += FLT_EPSILON;
	t.z += FLT_EPSILON;

	t = vec3(dot(t, rotsA[0]), dot(t, rotsA[1]), dot(t, rotsA[2]) );

	// compute common subexpressions, add epsilon to counter act
	// arithmetic errors when two edges are parallel ( avoid 0 )
	for ( int i = 0; i < 3; ++i )
		{
		for ( int j = 0; j < 3; ++j )
			{
			Absr[i][j] = abs(R[i][j]) + FLT_EPSILON;
			}
		}

	// Test axes L = A0, L = A1, L = A2
	for ( int i = 0; i < 3; ++i )
		{
		ra = extentsA[i];
		rb = extentsB[0] * Absr[i][0] + extentsB[1] * Absr[i][1] + extentsB[2] * Absr[i][2];
		
		if ( (abs(t[i]) > ra + rb)) 
			return 0;
		}
	
	// Test axes L = B0, L = B1, L = B2
	for ( int i = 0; i < 3; ++i )
		{
		ra = extentsA[0] * Absr[0][i] + extentsA[1] * Absr[1][i] + extentsA[2] * Absr[2][i];
		rb = extentsB[i];

		if (abs(t[0] * R[0][i] + t[1] * R[1][i] + t[2] * R[2][i]) > ra + rb)
			return 0;
		}
	
	// Test axis L = A0 x B0
	ra = extentsA[1] * Absr[2][0] + extentsA[2] * Absr[1][0];
	rb = extentsB[1] * Absr[0][2] + extentsB[2] * Absr[0][1];
	if (abs(t[2] * R[1][0] - t[1] * R[2][0]) > ra + rb)
		return 0;

	// Test axis L = A0 x B1
	ra = extentsA[1] * Absr[2][1] + extentsA[2] * Absr[1][1];
	rb = extentsB[0] * Absr[0][2] + extentsB[2] * Absr[0][0];
	if (abs(t[2] * R[1][1] - t[1] * R[2][1]) > ra + rb)
		return 0;

	// Test axis L = A0 x B2
	ra = extentsA[1] * Absr[2][2] + extentsA[2] * Absr[1][2];
	rb = extentsB[0] * Absr[0][1] + extentsB[1] * Absr[0][0];
	if (abs(t[2] * R[1][2] - t[1] * R[2][2]) > ra + rb)
		return 0;

	// Test axis L = A1 x B0
	ra = extentsA[0] * Absr[2][0] + extentsA[2] * Absr[0][0];
	rb = extentsB[1] * Absr[1][2] + extentsB[2] * Absr[1][1];
	if (abs(t[0] * R[2][0] - t[2] * R[0][0]) > ra + rb)
		return 0;
	
	// Test axis L = A1 x B1
	ra = extentsA[0] * Absr[2][1] + extentsA[2] * Absr[0][1];
	rb = extentsB[0] * Absr[1][2] + extentsB[2] * Absr[1][0];
	if (abs(t[0] * R[2][1] - t[2] * R[0][1]) > ra + rb)
		return 0;
	
	// Test axis L = A1 x B2
	ra = extentsA[0] * Absr[2][2] + extentsA[2] * Absr[0][2];
	rb = extentsB[0] * Absr[1][1] + extentsB[1] * Absr[1][0];
	if (abs(t[0] * R[2][2] - t[2] * R[0][2]) > ra + rb)
		return 0;
	
	// Test axis L = A2 x B0
	ra = extentsA[0] * Absr[1][0] + extentsA[1] * Absr[0][0];
	rb = extentsB[1] * Absr[2][2] + extentsB[2] * Absr[2][1];
	if (abs(t[1] * R[0][0] - t[0] * R[1][0]) > ra + rb) 
		return 0;
	
	// Test axis L = A2 x B1
	ra = extentsA[0] * Absr[1][1] + extentsA[1] * Absr[0][1];
	rb = extentsB[0] * Absr[2][2] + extentsB[2] * Absr[2][0];
	if (abs(t[1] * R[0][1] - t[0] * R[1][1]) > ra + rb)
		return 0;

	// Test axis L = A2 x B2
	ra = extentsA[0] * Absr[1][2] + extentsA[1] * Absr[0][2];
	rb = extentsB[0] * Absr[2][1] + extentsB[1] * Absr[2][0];
	if (abs(t[1] * R[0][2] - t[0] * R[1][2]) > ra + rb)
		return 0;

	// Since no separating axis is found, the OBBs must be intersecting
	return 1;
	}