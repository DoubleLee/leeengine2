/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Camera.h"

Camera::Camera()
	:
	mView(1.0f),
	mProj(1.0f),
    mPos(1.0f),
    mLookAt(1.0f),
    mUp(1.0f),
    mNearPlane(1.0f),
    mFarPlane(1000.0f),
    mFovy(1.0f),
    mAspect(1.0f),
	mNeedsRecalculate(true)
	{

	}

Camera::~Camera()
	{

	}

void Camera::SetView( const glm::vec3 & pos, const glm::vec3 & lookingAt, const glm::vec3 & up )
	{
	mNeedsRecalculate = true;
	mView = glm::lookAt(pos, lookingAt, up);
	mPos = pos;
	mLookAt = lookingAt;
	mUp = up;
	mForward = glm::normalize( mLookAt - mPos );

	mViewFrustum = ViewFrustum( mPos, mForward, mUp, mFovy, mAspect, mNearPlane, mFarPlane );
	}

void Camera::SetProj(float fovy, float aspect, float near, float far)
	{
	mNeedsRecalculate = true;
	mProj = glm::perspective( fovy, aspect, near, far );
	mNearPlane = near;
	mFarPlane = far;
	mFovy = fovy;
	mAspect = aspect;

	mViewFrustum = ViewFrustum( mPos, mForward, mUp, mFovy, mAspect, mNearPlane, mFarPlane );
	}

const glm::mat4 & Camera::GetProjView()
	{
	if ( mNeedsRecalculate )
		{
		mNeedsRecalculate = false;
		return ( mProjView = mProj * mView );
		}
	else
		return mProjView;
	}

const glm::mat4 & Camera::GetView() const
	{
	return mView;
	}

const glm::vec3 & Camera::GetPos() const
	{
	return mPos;
	}

const glm::vec3 & Camera::GetLookAt() const
	{
	return mLookAt;
	}

const glm::vec3 & Camera::GetUp() const
	{
	return mUp;
	}

const glm::vec3 Camera::GetForward() const
	{
	return mForward;
	}

Camera::operator const glm::mat4 & ()
	{
	return GetProjView();
	}

glm::mat4 Camera::operator * ( const glm::mat4 & other )
	{
	return GetProjView() * other;
	}
