/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "ViewFrustum.h"


ViewFrustum::ViewFrustum(void)
	{
	}

ViewFrustum::ViewFrustum( const vec3 & p, const vec3 & d, const vec3 & up, const float fov, const float ratio, const float nearDist, const float farDist )
	{
	float Hnear = 2 * tan(fov / 2) * nearDist;
	float Wnear = Hnear * ratio;

	float Hfar = 2 * tan(fov / 2) * farDist;
	float Wfar = Hfar * ratio;

	vec3 fc = p + d * farDist;

	vec3 right = cross( d, up );

	float two( 2.0f );

	vec3 ftl = fc + (up * Hfar/two) - (right * Wfar/two);

	vec3 ftr = fc + (up * Hfar/two) + (right * Wfar/two);
	vec3 fbl = fc - (up * Hfar/two) - (right * Wfar/two);
	vec3 fbr = fc - (up * Hfar/two) + (right * Wfar/two);

	vec3 nc = p + d * nearDist ;

	vec3 ntl = nc + (up * Hnear/two) - (right * Wnear/two);
	vec3 ntr = nc + (up * Hnear/two) + (right * Wnear/two);
	vec3 nbl = nc - (up * Hnear/two) - (right * Wnear/two);
	vec3 nbr = nc - (up * Hnear/two) + (right * Wnear/two);

	mPlanes[Planes::TOP] = Plane( ntl, ftl, ftr );
	mPlanes[Planes::RIGHT] = Plane( ntr, ftr, fbr );
	mPlanes[Planes::BOTTOM] = Plane( nbr, fbr, fbl );
	mPlanes[Planes::LEFT] = Plane( nbl, fbl, ftl );
	mPlanes[Planes::NEAR] = Plane( nbl, ntl, ntr );
	mPlanes[Planes::FAR] = Plane( ftr, ftl, fbl );
	}

ViewFrustum::~ViewFrustum(void)
	{

	}

bool ViewFrustum::ShouldBeCulled( const OrientedBB & obb )
	{
	for ( unsigned int i = 0; i < mPlanes.size(); ++i )
		{
		if ( mPlanes[i].GetPlaneBoxOrientation( obb ) == Orientation::NEGATIVE_SIDE )
			{
			return true;
			}
		}

	return false;
	}

array<Orientation, 6> ViewFrustum::GetPlaneOrientationsForOBB( const OrientedBB & obb )
	{
	array<Orientation, 6> orients;

	for ( unsigned int i = 0; i < orients.size(); ++i )
		{
		orients[i] = mPlanes[i].GetPlaneBoxOrientation( obb );
		}

	return std::move(orients);
	}
