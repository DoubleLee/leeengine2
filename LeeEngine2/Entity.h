/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"
using namespace glm;

class MeshData;
class TextureData;
class Camera;
class ShaderProgram;

class Entity
{
public:
	Entity(void);
	virtual ~Entity(void);

	// draws the entity
	void Draw( Camera * pCamera );

	mat4 mWorld;

	MeshData * mpMesh;
	TextureData * mpTexture;
	ShaderProgram * mpShaderProgram;
};

