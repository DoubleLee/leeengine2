/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"
using namespace glm;

// Because openGL is a single listener model
// this class emposes a singleton structure
// a static bool is kept to track if one of this class
// already exists, if you attempt to constuct another
// it throws an exception in the ctor.
class AudioListener
{
public:
	AudioListener();
	~AudioListener();

	// 0.0 - 1.0f 1.0f is 100%
	void SetMasterVolume( const float volume );
	void SetPosition( const vec3 & pos );
	void SetVelocity( const vec3 & vel);
	void SetOrientation( const vec3 & forward, const vec3 & up );

	float GetMasterVolume() const;
	vec3 GetPosition() const;
	vec3 GetVelocity() const;
	vec3 GetForward() const;
	vec3 GetUp() const;

private:
	float mGainVolume;
	vec3 mPos;
	vec3 mVel;
	vec3 mForward;
	vec3 mUp;

	static bool mSingletonListenerCreated;
};
