/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Resource.h"


Resource::Resource(void)
	:
	mIsLoaded(false),
	mFileName()
	{
	}


Resource::~Resource(void)
	{
	}

bool Resource::ReloadFromFile()
	{
	return LoadFromFile(mFileName);
	}

bool Resource::IsLoaded() const
	{
	return mIsLoaded;
	}

const string & Resource::GetFileName() const
	{
	return mFileName;
	}