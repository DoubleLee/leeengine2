#ifndef ENGINESETTINGSLOADER_H
#define ENGINESETTINGSLOADER_H

class EngineSettingsLoader
{
public:
	EngineSettingsLoader();
	virtual ~EngineSettingsLoader();

	virtual bool LoadSettings();

	unsigned int GetScreenWidth() const;
	unsigned int GetScreenHeight() const;

	unsigned int GetSamplesAA() const;
	unsigned int GetSamplesAF() const;

protected:
	unsigned int mScreenWidth;
	unsigned int mScreenHeight;

	// anti aliasing samples, 0 means disabled
	unsigned int mSamplesAA;
	// anisotropic filtering 0 means disabled
	float mSamplesAF;

};

#endif // ENGINESETTINGSLOADER_H
