/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "MeshData.h"

#include "Logger.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include <array>
#include <vector>

#include "GL/glew.h"

MeshData::MeshData()
	:
	mVAO(0),
	mVBOVertPositions(0),
	mVBOVertNormals(0),
	mVBOVertUVs(0),
	mVBOVertIndices(0),
	mVertCount(0),
	mVertIndexCount(0)
	{
	
	}


MeshData::~MeshData()
	{
	ReleaseResources();
	}

bool MeshData::LoadFromFile( const std::string & file )
	{
	ReleaseResources();

	Assimp::Importer importer;

	const aiScene * pScene = importer.ReadFile( file,
		/*aiProcess_FlipWindingOrder |*/ aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType );

	if ( !pScene || !pScene->HasMeshes())
		{
		LogError("Failed to load Mesh file " + file, importer.GetErrorString(), 0)
		return (mIsLoaded = false);
		}

	auto pMesh = pScene->mMeshes[0];

	mVertCount = pMesh->mNumVertices;

	mVertIndexCount = pMesh->mNumFaces * 3;

	std::vector<unsigned int> indices( mVertIndexCount, 0 );
	
	for ( unsigned int i = 0; i < mVertIndexCount; ++i )
		{
		indices.at(i) = pMesh->mFaces[ i / 3 ].mIndices[i % 3];
		}

	// extents for bounding box
	auto pVerts = pMesh->mVertices;
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	for ( unsigned int i = 0; i < mVertCount; ++i )
		{
		if ( x < abs(pVerts[i].x) )
			{
			x = abs(pVerts[i].x);
			}
		if ( y < abs(pVerts[i].y) )
			{
			y = abs(pVerts[i].y);
			}
		if ( z < abs(pVerts[i].z) )
			{
			z = abs(pVerts[i].z);
			}
		}

	mExtents.x = x;
	mExtents.y = y;
	mExtents.z = z;
	/*
	std::vector<aiVector2D> UVs( mVertCount, aiVector2D(0.f));

	for ( unsigned int i = 0; i < mVertCount; ++i )
		{
		UVs[i].x = pMesh->mTextureCoords[0][i].x;
		UVs[i].y = pMesh->mTextureCoords[0][i].y;
		}
	*/
	// create mesh vao
	glGenVertexArrays(1, &mVAO);
	glBindVertexArray(mVAO);
	// load positions
	glGenBuffers(1, &mVBOVertPositions);
	glBindBuffer( GL_ARRAY_BUFFER, mVBOVertPositions);

	glBufferData( GL_ARRAY_BUFFER, sizeof(pMesh->mVertices[0]) * pMesh->mNumVertices, pMesh->mVertices, GL_STATIC_DRAW );

	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(pMesh->mVertices[0]), (void*) 0 );

	// Load normals
	glGenBuffers(1, &mVBOVertNormals);
	glBindBuffer( GL_ARRAY_BUFFER, mVBOVertNormals );

	glBufferData( GL_ARRAY_BUFFER, sizeof( pMesh->mNormals[0]) * pMesh->mNumVertices, pMesh->mNormals, GL_STATIC_DRAW );
	
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(pMesh->mNormals[0]), (void*) 0 );

	// Load UVs
	/*
	glGenBuffers(1, &mVBOVertUVs );
	glBindBuffer( GL_ARRAY_BUFFER, mVBOVertUVs );

	glBufferData( GL_ARRAY_BUFFER, sizeof(UVs[0]) * pMesh->mNumVertices, UVs.data(), GL_STATIC_DRAW );

	glEnableVertexAttribArray(2);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(UVs[0]), (void*) 0 );
	*/
	glGenBuffers(1, &mVBOVertUVs );
	glBindBuffer( GL_ARRAY_BUFFER, mVBOVertUVs );

	glBufferData( GL_ARRAY_BUFFER, sizeof(pMesh->mTextureCoords[0][0]) * pMesh->mNumVertices, &pMesh->mTextureCoords[0][0], GL_STATIC_DRAW );

	glEnableVertexAttribArray(2);

	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(pMesh->mTextureCoords[0][0]), (void*) 0 );

	glGenBuffers(1, &mVBOVertIndices );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mVBOVertIndices );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * mVertIndexCount, indices.data(), GL_STATIC_DRAW );

	GLenum error = glGetError();

	if ( error != GL_NO_ERROR )
		{
		ReleaseResources();
		LogError("Failed while loading mesh " + file, (char*)glewGetErrorString(error), error)
		return (mIsLoaded = false);
		}

	LogSuccess("Mesh loaded " + file);
	mFileName = file;
	return (mIsLoaded = true);
	}

void MeshData::ReleaseResources()
	{
	glBindVertexArray(0);

	if ( mVAO )
		{
		glDeleteVertexArrays(1, &mVAO);
		mVAO = 0;
		}
		
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	if ( mVBOVertPositions )
		{
		glDeleteBuffers(1, &mVBOVertPositions);
		mVBOVertPositions = 0;
		}

	if ( mVBOVertNormals )
		{
		glDeleteBuffers(1, &mVBOVertNormals);
		mVBOVertNormals = 0;
		}
		
	if ( mVBOVertUVs )
		{
		glDeleteBuffers(1, &mVBOVertUVs);
		mVBOVertUVs = 0;
		}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	if( mVBOVertIndices )
		{
		glDeleteBuffers(1,&mVBOVertIndices);
		mVBOVertIndices = 0;
		}

	mVertCount = 0;
	mVertIndexCount = 0;

	mIsLoaded = false;
	}

void MeshData::Bind() const
	{
	glBindVertexArray(mVAO);
	}
