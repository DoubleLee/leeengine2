/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once


#include "glm/glm.hpp"
using namespace glm;

extern const float ZEROF;
extern const double ZEROD;

extern const float ONEF;
extern const double ONED;

extern const vec3 vec3up;
extern const vec3 vec3down;
extern const vec3 vec3left;
extern const vec3 vec3right;

extern const vec3 vec3front;
extern const vec3 vec3back;

extern const vec3 vec3zero;
extern const vec3 vec3one;

extern const mat4 mat4identity;
