/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "PlayerShip.h"

#include "Game.h"
#include "Engine.h"
#include "MathUtilities.h"
#include "AudioManager.h"
#include "AudioGameEnum.h"
#include "Window.h"

#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

PlayerShip::PlayerShip()
	:
	SafeEntity(),
	mpGame(nullptr)
	{

	}

bool PlayerShip::Init(Game *pGame)
	{
	mpGame = pGame;
	return true;
	}

void PlayerShip::UpdateControls(const float frameDelta, const float frameStamp)
	{
	Window * pWindow = mpGame->GetWindow();
	static const float playerSpeed = 50;
	vec3 moveVec(0.0f);
	if ( pWindow->IsKeyDown('W') )
		{
		moveVec.z -= 1.0f;
		}
	if ( pWindow->IsKeyDown('S') )
		{
		moveVec.z += 1.0f;
		}
	if ( pWindow->IsKeyDown('D') )
		{
		moveVec.x += 1.0f;
		}
	if ( pWindow->IsKeyDown('A') )
		{
		moveVec.x -= 1.0f;
		}

	if ( pWindow->IsKeyDown('Q') )
		{
		mWorld = rotate( mWorld, -(frameDelta * 3.14f) * 30.f, vec3front );
		}

	if ( pWindow->IsKeyDown('E') )
		{
		mWorld = rotate( mWorld, (frameDelta * 3.14f) * 30.f, vec3front );
		}

	// get current pos
	int x,y;
	pWindow->GetCursorPos( &x, &y );

	int sizeX, sizeY;
	pWindow->GetWindowSize( &sizeX, &sizeY );

	ivec2 center( sizeX, sizeY );
	center /= 2;

	ivec2 offCenter( center - ivec2(x,y) );

	if ( offCenter.x )
		{
		mWorld = rotate( mWorld, float(offCenter.x) / 3.14f, vec3up );
		}
	if ( offCenter.y )
		{
		mWorld = rotate( mWorld, float(offCenter.y) / 3.14f, vec3right );
		}

	pWindow->SetCursorPos( center.x, center.y );


	mWorld = translate( mWorld, moveVec * (playerSpeed * frameDelta) );

	static float shootTrigger = 0.0f;
	if ( pWindow->IsMousebuttonDown( GLFW_MOUSE_BUTTON_LEFT ) && shootTrigger < frameStamp )
		{
		vec3 & pos = (vec3&)mWorld[3];
		mpGame->mpAudioMan->PlayRegisteredAudio( Registered::LaserShot, pos, false);
		shootTrigger = frameStamp + 0.5f;
		}
	}
