/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Planet.h"

#include "Game.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

Planet::Planet()
	:
	SafeEntity(),
	mSpinSpeed(0.0f),
	mpGame(nullptr)
	{

	}

bool Planet::Init(Game * pGame, float spinSpeed )
	{
	mpGame = pGame;
	mSpinSpeed = spinSpeed;
	return true;
	}
