/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include <iostream>
using namespace std;

#include "Game.h"

int main()
    {
    Game game;
    if ( game.Init() )
        return game.Run();
    else
        return 1;
    }

