/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "MapLoaderFlyableShip.h"

#include "Engine.h"
#include "Game.h"
#include "PlayerShip.h"
#include "Planet.h"
#include "MathUtilities.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <fstream>
using namespace std;

MapLoaderFlyableShip::MapLoaderFlyableShip()
	:
	mpGame(nullptr)
	{

	}

MapLoaderFlyableShip::~MapLoaderFlyableShip()
	{

	}

bool MapLoaderFlyableShip::LoadMap(Game *pGame, string file)
	{
	if ( !pGame  )
		return false;

	mpGame = pGame;

	return TextMapLoader::LoadMap(mpGame, file);
	}

FindResult MapLoaderFlyableShip::FindVirtualCommand(string & command, string & data, ifstream & ifs)
	{
	if ( command == "player" )
		{
		unsigned int meshIndex, texIndex, programIndex;
		ifs >> meshIndex;
		ifs >> texIndex;
		ifs >> programIndex;

		vec3 pos;
		ifs >> pos.x;
		ifs >> pos.y;
		ifs >> pos.z;

		if ( ifs.bad() )
			{
			return FindResult::ResultError;
			}

		UniquePointer<PlayerShip> pEnt( new PlayerShip() );

		pEnt->Init(mpGame);

		pEnt->mWorld = translate( mat4identity, pos );

		pEnt->SetMesh( mMeshes[meshIndex] );
		pEnt->SetTexture( mTextures[texIndex] );
		pEnt->SetProgram( mPrograms[programIndex] );

		mpGame->mpPlayer = std::move(pEnt);

		return FindResult::ResultFound;
		}
	else if ( command == "entityArray" )
		{
		unsigned int meshIndex, texIndex, programIndex, count;
		float stepX, stepY;

		ifs >> meshIndex;
		ifs >> texIndex;
		ifs >> programIndex;
		ifs >> count;
		ifs >> stepX;
		ifs >> stepY;

		if ( ifs.bad() )
			{
			return FindResult::ResultError;
			}

		for ( unsigned int i = 0; i < count; ++i )
			{
			for ( unsigned int j = 0; j < count; ++j )
				{
				SharedPointer<SafeEntity> pEnt(new SafeEntity() );
				pEnt->mWorld = translate( mat4identity, vec3(float(i) * stepX, 0.0f, float(j) * stepY) );
				pEnt->SetMesh( mMeshes[meshIndex] );
				pEnt->SetTexture( mTextures[texIndex] );
				pEnt->SetProgram( mPrograms[programIndex] );

				mpGame->mEntities.push_back( pEnt );
				}
			}
		return FindResult::ResultFound;
		}
	else if ( command == "planet" )
		{
		unsigned int meshIndex, texIndex, programIndex;
		ifs >> meshIndex;
		ifs >> texIndex;
		ifs >> programIndex;

		vec3 pos;
		ifs >> pos.x;
		ifs >> pos.y;
		ifs >> pos.z;

		vec3 scales;
		ifs >> scales.x;
		scales.y = scales.x;
		scales.z = scales.x;

		float spinSpeed;
		ifs >> spinSpeed;

		if ( ifs.bad() )
			{
			return FindResult::ResultError;
			}

		Planet * pEnt = new Planet();
		pEnt->Init( mpGame, spinSpeed );
		pEnt->mWorld = translate( mat4identity, vec3(pos) );
		pEnt->mWorld = scale(pEnt->mWorld, scales);
		pEnt->SetMesh( mMeshes[meshIndex] );
		pEnt->SetTexture( mTextures[texIndex] );
		pEnt->SetProgram( mPrograms[programIndex] );

		mpGame->mEntities.emplace_back( pEnt );
		return FindResult::ResultFound;
		}
	else
		{
		return TextMapLoader::FindVirtualCommand(command, data, ifs);
		}
	}
