TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += \
	Game.h \
	MapLoaderFlyableShip.h \
	PlayerShip.h \
	Planet.h \
	AudioGameEnum.h

SOURCES += main.cpp \
	Game.cpp \
	MapLoaderFlyableShip.cpp \
	PlayerShip.cpp \
	Planet.cpp

unix:!macx: LIBS += -L$$OUT_PWD/../LeeEngine2/ -lLeeEngine2

INCLUDEPATH += $$PWD/../LeeEngine2
DEPENDPATH += $$PWD/../LeeEngine2

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../LeeEngine2/libLeeEngine2.a

unix:!macx: LIBS += -L/usr/lib64/ -lGL -lGLU -lGLEW -lglfw3 -lfreeimage -lassimp -lopenal -lalut -lXi -lX11 -lXxf86vm -lpthread -lrt -lXrandr -ldl

unix:!macx: QMAKE_CXXFLAGS += -std=c++11

