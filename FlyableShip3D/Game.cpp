/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#include "Game.h"

#include "Logger.h"
#include "Engine.h"
#include "Skybox.h"
#include "Planet.h"
#include "PlayerShip.h"
#include "SafeEntity.h"
#include "Camera.h"
#include "MapLoaderFlyableShip.h"
#include "MeshData.h"
#include "TextureData.h"
#include "ShaderProgram.h"
#include "MathUtilities.h"
#include "AudioGameEnum.h"
#include "AudioManager.h"
#include "EngineSettingsLoader.h"
#include "Window.h"

#include "GLFW/glfw3.h"

Game::Game()
	:
	Engine(),
	mpPlayer(nullptr)
	{

	}

Game::~Game()
	{

	}

bool Game::Init()
	{
	if ( !Engine::Init( 1680, 1050 ) )
		{
		LogFailure("Failed to init engine.")
		return false;
		}

	// Register enums

	MapLoaderFlyableShip loader;
	if ( !loader.LoadMap(this, "../../LeeEngine2SubDirs/Art/Maps/FlyableShipMap1.txt") )
		{
		LogFailure("Failed to load map.")
		return false;
		}

	int x, y;
	mpWindow->GetWindowSize( &x, &y );

	mpWindow->SetCursorPos( x / 2, y / 2 );

	return true;
	}

void Game::GameUpdate()
	{
	if ( mpWindow->IsKeyDown( GLFW_KEY_ESCAPE ) )
		{
		mpWindow->CloseWindow();
		return;
		}
	mWindowTitle += "FPS ";
	mWindowTitle += std::to_string(mFramesPerSec);

	mpPlayer->UpdateControls( mFrameDeltaF, mFrameStampF );

	// setup camera
	vec3 pos = (vec3&)mpPlayer->mWorld[3];
	pos += (vec3&)mpPlayer->mWorld[2] * 6.0f;
	pos += (vec3&)mpPlayer->mWorld[1] * 2.0f;
	vec3 lookat = (vec3&)mpPlayer->mWorld[3] + ((vec3&)mpPlayer->mWorld[2] * -4.0f);
	vec3 forward = normalize(pos - lookat);
	vec3 right = (vec3&)mpPlayer->mWorld[0];
	vec3 up = cross( forward, right );
	mpCamera->SetView( pos, lookat, up );

	mpPlayer->UpdateOBB();
	for ( unsigned int i = 0; i < mEntities.size(); ++i )
		{
		SharedPointer<Planet> pPlanet = std::dynamic_pointer_cast<Planet> ( mEntities[i] );
		if ( pPlanet )
			{
			pPlanet->mWorld = rotate(pPlanet->mWorld, pPlanet->mSpinSpeed * mFrameDeltaF, vec3up );
			}

		mEntities[i]->UpdateOBB();
		}

	// TODO: maybe shouldn't negate forward, seems to work though
	mpAudioMan->SetListenerPosAndOrient( pos, -forward, up );
	}

void Game::GameDraw()
	{
	// Draw Skybox
	mpSkybox->Draw(mpCamera.get());
	// Draw player
	if ( mpPlayer )
		mpPlayer->Draw( mpCamera.get() );

	// Draw entities
	int culledObjs = 0;
	for( unsigned int i = 0; i < mEntities.size(); ++i )
		{
		if ( !mpCamera->mViewFrustum.ShouldBeCulled( *mEntities[i]->mpOBB ) )
			{
			mEntities[i]->Draw( mpCamera.get() );
			}
		else
			{
			++culledObjs;
			}
		}
	if ( culledObjs )
		{
		mWindowTitle += " ObjectsCulled ";
		mWindowTitle += std::to_string(culledObjs);
		}
	}

void Game::RegisterAudioGameEnums()
	{
	for ( int i = Registered::FirstAudioGame + 1; i < Registered::LastAudioGame; ++i )
		{
		mpAudioMan->AddRegisteredName( i );
		}
	}
