/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#ifndef PLAYERSHIP_H
#define PLAYERSHIP_H

#include "SafeEntity.h"

class Game;

class PlayerShip : public SafeEntity
{
public:
    PlayerShip();
    bool Init( Game * pGame );
	void UpdateControls( const float frameDelta, const float frameStamp );

protected:
    Game * mpGame;
};

#endif // PLAYERSHIP_H
