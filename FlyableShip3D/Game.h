/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#ifndef GAME_H
#define GAME_H

#include "SmartPointers.h"
class PlayerShip;

#include "Engine.h"

class Game : public Engine
{
public:
	Game();
	virtual ~Game();
	virtual bool Init();

	virtual void GameUpdate();
	virtual void GameDraw();
	virtual void RegisterAudioGameEnums();
protected:

public:
	UniquePointer<PlayerShip> mpPlayer;
private:
};

#endif // GAME_H
