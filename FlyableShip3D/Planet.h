/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#ifndef PLANET_H
#define PLANET_H

#include "SafeEntity.h"

class Game;

class Planet : public SafeEntity
{
public:
	Planet();

	bool Init( Game * pGame, float spinSpeed );

	float mSpinSpeed;
	Game * mpGame;
};

#endif // PLANET_H
