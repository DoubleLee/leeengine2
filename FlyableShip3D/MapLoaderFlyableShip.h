/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#ifndef MAPLOADERFLYABLESHIP_H
#define MAPLOADERFLYABLESHIP_H

#include "TextMapLoader.h"

class Game;

class MapLoaderFlyableShip : public TextMapLoader
{
public:
	MapLoaderFlyableShip();
	virtual ~MapLoaderFlyableShip();

	bool LoadMap(Game * pGame, string file);

	virtual FindResult FindVirtualCommand(string & command, string & data, ifstream & ifs);

protected:
	Game * mpGame;
};

#endif // MAPLOADERFLYABLESHIP_H
