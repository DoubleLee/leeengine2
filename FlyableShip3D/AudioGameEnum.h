/*
Copyright 2013 @ Chris Leisure
All rights reserved.
*/

#pragma once

#ifndef AUDIOGAMEENUM_H
#define AUDIOGAMEENUM_H

#include "AudioEngineEnum.h"

namespace Registered
{
enum AudioGame
{
FirstAudioGame = LastAudioEngine + 1,
LaserShot,
Explosion,
LastAudioGame
};
}

#endif // AUDIOGAMEENUM_H
